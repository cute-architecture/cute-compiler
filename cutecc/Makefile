# Copyright © Romain Fouquet, 2020
#
# This file is part of the CUTE compiler.
#
# The CUTE compiler is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# The CUTE compiler is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with The CUTE compiler.  If not, see <https://www.gnu.org/licenses/>.

TEST_DIR = tests
FILE = led_blink
OPT_SUFFIX = _opt

IR_ARGS = --target=arm -emit-llvm -Xclang -disable-O0-optnone
OPT_ARGS = --lowerswitch --licm --mem2reg --ipsccp --dse \
					 --loop-rotate \
					 --partial-inliner --inline --sink --memcpyopt \
					 --simplifycfg \
					 --instcombine --aggressive-instcombine \
					 --mem2reg

compile2ir:
	(cd $(TEST_DIR) \
	&& clang $(IR_ARGS) -S $(FILE).c \
	&& clang $(IR_ARGS) -c $(FILE).c -o $(FILE).bc \
	&& opt $(OPT_ARGS) -S $(FILE).ll -o $(FILE)$(OPT_SUFFIX).ll \
	&& llvm-as $(FILE)$(OPT_SUFFIX).ll -o $(FILE)$(OPT_SUFFIX).bc)
