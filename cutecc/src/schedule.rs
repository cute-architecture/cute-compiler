/*
 * Copyright © Romain Fouquet, 2020
 *
 * This file is part of the CUTE compiler.
 *
 * The CUTE compiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The CUTE compiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with The CUTE compiler.  If not, see <https://www.gnu.org/licenses/>.
 */

use llvm_ir::instruction;
use llvm_ir::{Constant, ConstantRef, Function, Instruction, Module, Name, Operand};

use super::llvm_ir_utils::get_operands::GetOperands;

fn get_nop_instruction(module: &Module) -> Instruction {
    Instruction::BitCast(instruction::BitCast {
        operand: Operand::ConstantOperand(ConstantRef::new(Constant::Int { value: 0, bits: 32 })),
        to_type: module.types.i32(),
        dest: Name::Number(0),
        debugloc: None,
    })
}

fn schedule_next_instruction(module: &Module, function: &Function) -> Option<Function> {
    for (bi, basic_block) in function.basic_blocks.iter().enumerate() {
        for (ii, instruction) in basic_block.instrs.iter().enumerate() {
            if let Some(res) = instruction.try_get_result() {
                // FIXME: check the term instruction of there is no next regular instruction
                if let Some(next_instr) = basic_block.instrs.get(ii + 1) {
                    // If the next instruction uses the result of the current instruction
                    // FIXME: check the next one too
                    if next_instr.get_operands().iter().any(|op| match op {
                        Operand::LocalOperand { name, .. } => name == res,
                        _ => false,
                    }) {
                        let mut new_function = function.clone();
                        // Add two NOPs
                        // Do not use insert_instruction, as it would create a result for this NOP
                        new_function.basic_blocks[bi]
                            .instrs
                            .insert(ii + 1, get_nop_instruction(module));
                        new_function.basic_blocks[bi]
                            .instrs
                            .insert(ii + 1, get_nop_instruction(module));
                        return Some(new_function);
                    }
                }
            }
        }
    }

    None
}

pub trait Schedule {
    fn schedule(&self, module: &Module) -> Function;
}

impl Schedule for Function {
    fn schedule(&self, module: &Module) -> Function {
        let mut scheduled_function = self.clone();

        while let Some(new_function) = schedule_next_instruction(module, &scheduled_function) {
            scheduled_function = new_function;
        }

        scheduled_function
    }
}
