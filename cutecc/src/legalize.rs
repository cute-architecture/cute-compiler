/*
 * Copyright © Romain Fouquet, 2020
 *
 * This file is part of the CUTE compiler.
 *
 * The CUTE compiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The CUTE compiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with The CUTE compiler.  If not, see <https://www.gnu.org/licenses/>.
 */

use std::collections::HashMap;

use llvm_ir::constant;
use llvm_ir::instruction::{self, BinaryOp};
use llvm_ir::{Constant, ConstantRef, Function, Instruction, Module, Name, Operand};

use cuteisa::{
    can_be_lower_immediate_value, can_be_short_immediate_value, can_be_upper_immediate_value,
};
use cuteisa::{MEM_FLASH_OFFSET, MEM_FLASH_OFFSET_START_CODE};

use super::get_add_instruction;
use super::llvm_ir_utils::can_branch_to::CanBranchTo;
use super::llvm_ir_utils::get_operands::{GetOperands, GetOperandsMut};
use super::llvm_ir_utils::{InsertInstruction, RemoveInstruction};

type Offset = u32; // FIXME: ?
type SymbolTable = HashMap<Name, Offset>;

fn get_int_operand(constant: u64) -> Operand {
    Operand::ConstantOperand(ConstantRef::new(Constant::Int {
        value: constant,
        bits: 32,
    }))
}

fn legalize_get_element_ptr_instruction(
    module: &Module,
    function: &Function,
    get_element_ptr: &instruction::GetElementPtr,
    bi: usize,
    ii: usize,
    text_size_bytes: u32,
    rodata_symbol_table: &SymbolTable,
) -> Option<Function> {
    let instruction::GetElementPtr { indices, .. } = get_element_ptr;

    // FIXME: hoist all instructions involved in producing the address in the preds
    // FIXME: find the common ancestor, or add the instructions to all the BBs branching to
    // this one
    let ancestor_block_index = function
        .basic_blocks
        .iter()
        .position(|b| b.can_branch_to(&function.basic_blocks[bi]))
        .unwrap();
    // let ancestor_block_index = match &indices[1] {
    //     Operand::LocalOperand { name, .. } =>
    //         function.basic_blocks
    //         .iter()
    //         .position(|b|
    //             b.instrs
    //             .iter()
    //             .any(|i| i.try_get_result().map_or(false, |r| r == name))
    //         )
    //         .unwrap(),
    //     _ => 0,
    // };

    // dbg!(&ancestor_block_index);

    let mut new_function = function.clone();

    let (base_offset_name, offset_operand) = match &get_element_ptr.address {
        Operand::ConstantOperand(cref) => match cref.as_ref() {
            Constant::GlobalReference { name, .. } => {
                let symbol_bin_offset = text_size_bytes + rodata_symbol_table.get(name).unwrap();

                // FIXME: this works only if the first index == 0

                // If the value cannot fit as immediate, it will be split at the next legalization
                // iteration
                let add_instruction_symbol = get_add_instruction(
                    Name::Number(0),    // Set when inserted
                    get_int_operand(0), // L0 == 0
                    get_int_operand(
                        (MEM_FLASH_OFFSET + MEM_FLASH_OFFSET_START_CODE + symbol_bin_offset).into(),
                    ),
                );

                let add_instruction_symbol = new_function.insert_instruction(
                    ancestor_block_index,
                    new_function.basic_blocks[ancestor_block_index].instrs.len(),
                    &add_instruction_symbol,
                );

                (
                    add_instruction_symbol.try_get_result().unwrap().clone(),
                    indices[1].clone(),
                )
            }
            _ => unreachable!(),
        },
        Operand::LocalOperand { name, .. } => (name.clone(), indices[0].clone()),
        _ => unimplemented!(),
    };

    dbg!(&get_element_ptr);

    // TODO: simplify if the offset is constant

    let add_instruction_offset = get_add_instruction(
        Name::Number(0), // Set when inserted
        Operand::LocalOperand {
            name: base_offset_name,
            ty: module.types.i32(), // FIXME: check this
        },
        offset_operand, // FIXME
    );

    let add_instruction_offset = new_function.insert_instruction(
        ancestor_block_index,
        new_function.basic_blocks[ancestor_block_index].instrs.len(),
        &add_instruction_offset,
    );

    // FIXME: refactor this, find the right instruction
    let load_store_instruction = &mut new_function.basic_blocks[bi].instrs[ii + 1];
    let address_name = match load_store_instruction {
        Instruction::Load(instruction::Load {
            address: Operand::LocalOperand { name, .. },
            ..
        }) => name,
        Instruction::Store(instruction::Store {
            address: Operand::LocalOperand { name, .. },
            ..
        }) => name,
        _ => unimplemented!(),
    };
    *address_name = add_instruction_offset.try_get_result().unwrap().clone();

    new_function.remove_instruction(bi, ii, None);

    Some(new_function)
}

// TODO: factor out these functions
fn legalize_store_instruction(
    module: &Module,
    function: &Function,
    store: &instruction::Store,
    bi: usize,
    ii: usize,
) -> Option<Function> {
    // TODO: use this instead when the box pattern becomes stable
    // Operand::ConstantOperand(Constant::IntToPtr(box constant::IntToPtr {
    //     operand: Constant::Int {value, ..},
    //     ..
    // }))

    // FIXME: this currently prevents the use of STRI
    if let Some(Constant::Int { value, .. }) = &store.value.as_constant() {
        // If the value cannot fit as immediate, it will be split at the next legalization
        // iteration
        let add_instruction = Instruction::Add(instruction::Add {
            operand0: get_int_operand(0), // FIXME
            operand1: get_int_operand(*value),
            dest: Name::Number(0),
            debugloc: None,
        });

        let mut new_function = function.clone();

        let add_result = new_function.insert_instruction(bi, ii, &add_instruction);

        *new_function.basic_blocks[bi].instrs[ii + 1].get_operands_mut()[1] =
            Operand::LocalOperand {
                name: add_result.try_get_result().unwrap().clone(),
                ty: module.types.i32(), // FIXME: check this
            };

        return Some(new_function);
    }

    if let Some(Constant::IntToPtr(constant)) = &store.address.as_constant() {
        let constant::IntToPtr { operand, .. } = constant;
        if let Constant::Int { value, .. } = operand.as_ref() {
            let add_instruction = Instruction::Add(instruction::Add {
                operand0: get_int_operand(0), // FIXME
                operand1: get_int_operand(*value),
                dest: Name::Number(0),
                debugloc: None,
            });

            let mut new_function = function.clone();

            let add_result = new_function.insert_instruction(bi, ii, &add_instruction);

            *new_function.basic_blocks[bi].instrs[ii + 1].get_operands_mut()[0] =
                Operand::LocalOperand {
                    name: add_result.try_get_result().unwrap().clone(),
                    ty: module.types.pointer_to(module.types.i32()),
                };

            return Some(new_function);
        }
    }

    None
}

fn legalize_icmp_instruction(
    module: &Module,
    function: &Function,
    icmp: &instruction::ICmp,
    bi: usize,
    ii: usize,
) -> Option<Function> {
    let instruction::ICmp { operand1, .. } = icmp;
    if let Some(Constant::Int { value, .. }) = operand1.as_constant() {
        // TODO: debug this
        if !can_be_short_immediate_value(*value) {
            let add_instruction = Instruction::Add(instruction::Add {
                operand0: get_int_operand(0), // FIXME
                operand1: get_int_operand(*value),
                dest: Name::Number(0),
                debugloc: None,
            });

            let mut new_function = function.clone();

            let add_result = new_function.insert_instruction(bi, ii, &add_instruction);

            *new_function.basic_blocks[bi].instrs[ii + 1].get_operands_mut()[1] =
                Operand::LocalOperand {
                    name: add_result.try_get_result().unwrap().clone(),
                    ty: module.types.i32(), // FIXME: check this
                };

            return Some(new_function);
        }
    }

    None
}

fn legalize_add_instruction(
    module: &Module,
    function: &Function,
    add: &instruction::Add,
    bi: usize,
    ii: usize,
) -> Option<Function> {
    if let Some(Constant::Int { value, .. }) = add.get_operand1().as_constant() {
        if !can_be_lower_immediate_value(*value) && !can_be_upper_immediate_value(*value) {
            let add_instruction = Instruction::Add(instruction::Add {
                operand0: get_int_operand(0), // FIXME
                operand1: get_int_operand((value >> 16) << 16),
                dest: Name::Number(0),
                debugloc: None,
            });

            let mut new_function = function.clone();

            // Insert add instruction for upper half word initialization
            let add_result = new_function.insert_instruction(bi, ii, &add_instruction);

            // Update the original add instruction to use use the added upper half word
            *new_function.basic_blocks[bi].instrs[ii + 1].get_operands_mut()[0] =
                Operand::LocalOperand {
                    name: add_result.try_get_result().unwrap().clone(),
                    ty: module.types.i32(), // FIXME: check this
                };

            // Update the immediate value of the original add instruction so that it is only the
            // lower half word
            *new_function.basic_blocks[bi].instrs[ii + 1].get_operands_mut()[1] =
                get_int_operand(value & 0xffff);

            return Some(new_function);
        }
    }

    None
}

fn legalize_next_instruction(
    module: &Module,
    function: &Function,
    text_size_bytes: u32,
    rodata_symbol_table: &SymbolTable,
) -> Option<Function> {
    // println!("{}", function.print_ir());
    for (bi, basic_block) in function.basic_blocks.iter().enumerate() {
        for (ii, instruction) in basic_block.instrs.iter().enumerate() {
            match instruction {
                Instruction::Add(add) => {
                    if let Some(new_function) =
                        legalize_add_instruction(module, function, add, bi, ii)
                    {
                        return Some(new_function);
                    }
                }
                Instruction::GetElementPtr(get_element_ptr) => {
                    if let Some(new_function) = legalize_get_element_ptr_instruction(
                        module,
                        function,
                        get_element_ptr,
                        bi,
                        ii,
                        text_size_bytes,
                        rodata_symbol_table,
                    ) {
                        return Some(new_function);
                    }
                }
                Instruction::Store(store) => {
                    if let Some(new_function) =
                        legalize_store_instruction(module, function, store, bi, ii)
                    {
                        return Some(new_function);
                    }
                }
                Instruction::ICmp(icmp) => {
                    if let Some(new_function) =
                        legalize_icmp_instruction(module, function, icmp, bi, ii)
                    {
                        return Some(new_function);
                    }
                }
                Instruction::ZExt(_) => {
                    // TODO: maybe ZExt instructions should not be removed
                    let mut new_function = function.clone();
                    let operand_zext = instruction.get_operands()[0];
                    new_function.remove_instruction(bi, ii, Some(operand_zext));
                    return Some(new_function);
                }
                _ => {}
            }
        }
    }

    None
}

pub trait Legalize {
    fn legalize(
        &self,
        module: &Module,
        text_size_bytes: u32,
        rodata_symbol_table: &SymbolTable,
    ) -> Function;
}

impl Legalize for Function {
    fn legalize(
        &self,
        module: &Module,
        text_size_bytes: u32,
        rodata_symbol_table: &SymbolTable,
    ) -> Function {
        let mut legalized_function = self.clone();

        while let Some(new_function) = legalize_next_instruction(
            module,
            &legalized_function,
            text_size_bytes,
            rodata_symbol_table,
        ) {
            legalized_function = new_function;
        }

        legalized_function
    }
}
