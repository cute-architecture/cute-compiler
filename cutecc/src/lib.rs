/*
 * Copyright © Romain Fouquet, 2020
 *
 * This file is part of the CUTE compiler.
 *
 * The CUTE compiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The CUTE compiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with The CUTE compiler.  If not, see <https://www.gnu.org/licenses/>.
 */

use std::collections::HashMap;

use llvm_ir::instruction;
use llvm_ir::{BasicBlock, Constant, ConstantRef, Function, Instruction, Module, Name, Operand};

mod llvm_ir_utils;

mod get_code_size_bytes;
mod legalize;
mod schedule;

pub mod print_asm;

use llvm_ir_utils::can_branch_to::CanBranchTo;
use llvm_ir_utils::get_operands::{GetOperands, GetOperandsMut};
use llvm_ir_utils::IsDefinedUsedIn;
use llvm_ir_utils::{InsertInstruction, RemoveInstruction};

const GLOBAL_REGISTER_NAME_PREFIX: &str = "gReg";
// FIXME: find a better solution
const LOAD_NAME_PREFIX: &str = "LDR";

pub fn get_add_instruction(dest: Name, operand0: Operand, operand1: Operand) -> Instruction {
    Instruction::Add(instruction::Add {
        operand0,
        operand1,
        dest,
        debugloc: None,
    })
}

pub fn get_write_instruction(
    module: &Module,
    global_reg_name: &Name,
    local_reg_name: &Name,
) -> Instruction {
    Instruction::Store(instruction::Store {
        address: Operand::ConstantOperand(ConstantRef::new(Constant::GlobalReference {
            name: global_reg_name.clone(), // FIXME: avoid conflicts with other global values
            ty: module.types.i32(),
        })),
        value: Operand::LocalOperand {
            name: local_reg_name.clone(),
            ty: module.types.i32(),
        },
        volatile: false, // Does not matter
        atomicity: None,
        alignment: 4,
        debugloc: None,
    })
}

fn get_store_instruction(module: &Module, value: &Constant, global_reg_name: &Name) -> Instruction {
    Instruction::Store(instruction::Store {
        address: Operand::ConstantOperand(ConstantRef::new(Constant::GlobalReference {
            name: global_reg_name.clone(),
            ty: module.types.i32(),
        })),
        value: Operand::ConstantOperand(ConstantRef::new(match value {
            Constant::Int { .. } => value.clone(),
            Constant::Undef(_) => Constant::Int { value: 0, bits: 32 },
            _ => unimplemented!(),
        })),
        volatile: false, // Does not matter
        atomicity: None,
        alignment: 4,
        debugloc: None,
    })
}

fn get_read_instruction(module: &Module, global_reg_name: &Name) -> Instruction {
    Instruction::Load(instruction::Load {
        address: Operand::ConstantOperand(ConstantRef::new(Constant::GlobalReference {
            name: global_reg_name.clone(),
            ty: module.types.i32(),
        })),
        dest: Name::Number(0), // Will be set properly when inserted
        volatile: false,       // Does not matter
        atomicity: None,
        alignment: 4,
        debugloc: None,
    })
}

trait InsertReadsWrites {
    fn insert_reads_writes(&self, module: &Module) -> Function;
}

// TODO: split this function
impl InsertReadsWrites for Function {
    fn insert_reads_writes(&self, module: &Module) -> Function {
        let mut new_function = self.clone();
        let mut write_count = 0;
        let mut global_reg_refs = HashMap::new();

        for param in &self.parameters {
            global_reg_refs.insert(
                &param.name,
                // FIXME: allocate in global registers reserved for function params,
                // defined in the ABI
                write_count,
            );

            write_count += 1; // TODO: rename write_count
        }

        // Insert store instructions as WRITE instructions for results used in another basic block
        for (bi, basic_block) in self.basic_blocks.iter().enumerate() {
            let mut write_in_block_count = 0;
            for (ii, instr) in basic_block.instrs.iter().enumerate() {
                if let Some(name) = instr.try_get_result() {
                    // FIXME: maybe do insert WRITE for loops, to avoid removing them afterwards
                    // If the result is used in another basic block or in the same one that loops,
                    // with a phi instruction
                    if self
                        .basic_blocks
                        .iter()
                        .any(|bb| bb != basic_block && name.is_used_in(bb))
                        || basic_block.instrs.iter().any(|i| match i {
                            Instruction::Phi(_) => i.get_operands().iter().any(|op| match op {
                                Operand::LocalOperand { name: op_name, .. } => op_name == name,
                                _ => false,
                            }),
                            _ => false,
                        })
                    {
                        let store_instruction = get_write_instruction(
                            module,
                            &Name::Name(Box::new(format!(
                                "{}{}",
                                GLOBAL_REGISTER_NAME_PREFIX, write_count
                            ))),
                            &name.clone(),
                        );

                        new_function.insert_instruction(
                            bi,
                            ii + 1 + write_in_block_count,
                            &store_instruction,
                        );

                        global_reg_refs.insert(name, write_count);
                        write_count += 1;
                        write_in_block_count += 1;
                    }
                }
            }
        }

        // dbg!(&global_reg_refs);

        let placeholder_name = Name::Name(Box::new(format!("{}4242", GLOBAL_REGISTER_NAME_PREFIX)));

        // Insert store instructions as WRITE instructions for Phi constants in other destination
        // blocks
        let mut store_consts = vec![];
        for basic_block in &self.basic_blocks {
            for instr in &basic_block.instrs {
                match instr {
                    Instruction::Phi(instruction::Phi {
                        incoming_values, ..
                    }) => {
                        if incoming_values.len() > 2 {
                            // TODO: maybe this may be removed
                            panic!(
                                "Phi instructions with more than 2 preds are not yet supported."
                            );
                        }

                        let global_reg_name = incoming_values
                            .iter()
                            .find(|(op, _)| matches!(op, Operand::LocalOperand { .. }));
                        let global_reg_name = Name::Name(Box::new(format!(
                            "{}{}",
                            GLOBAL_REGISTER_NAME_PREFIX,
                            match global_reg_name {
                                Some((Operand::LocalOperand { name, .. }, _)) => {
                                    *global_reg_refs.get(&name).unwrap()
                                }
                                _ => {
                                    let global_reg =
                                        *global_reg_refs.iter().map(|(_, c)| c).max().unwrap_or(&0)
                                            + 1;
                                    // This a placeholder value to mark global registers allocated only for constants
                                    global_reg_refs.insert(&placeholder_name, global_reg);
                                    global_reg
                                }
                            }
                        )));

                        for (op, n) in incoming_values {
                            if let Operand::ConstantOperand(constant) = op {
                                store_consts.push((
                                    n.clone(),
                                    constant.clone(),
                                    global_reg_name.clone(),
                                ));
                            }
                        }
                    }
                    _ => break, // Phi instructions must come first in the basic block
                }
            }
        }
        // dbg!(&store_consts);
        for store_const in &store_consts {
            let store_instruction = get_store_instruction(module, &store_const.1, &store_const.2);

            let bi = new_function
                .basic_blocks
                .iter()
                .position(|bb| bb.name == store_const.0)
                .unwrap();

            new_function.insert_instruction(
                bi,
                new_function.basic_blocks[bi].instrs.len(),
                &store_instruction,
            );
        }

        // dbg!(&global_reg_refs);

        // Insert load instructions as READ instructions
        for (bi, basic_block) in self.basic_blocks.iter().enumerate() {
            let mut load_count = 0;

            for (ii, instr) in basic_block.instrs.iter().enumerate() {
                match instr {
                    Instruction::Phi(instruction::Phi {
                        incoming_values, ..
                    }) => {
                        // If all incoming values are constants
                        if incoming_values
                            .iter()
                            .all(|(op, _)| matches!(op, Operand::ConstantOperand(_)))
                        {
                            let load_instruction = get_read_instruction(
                                module,
                                &store_consts
                                    .iter()
                                    .find(|(n, constant, _)| {
                                        incoming_values[0].1 == *n
                                            && incoming_values[0].0
                                                == Operand::ConstantOperand(constant.clone())
                                    })
                                    .unwrap()
                                    .2
                                    .clone(),
                            );

                            let inserted_load_instruction =
                                new_function.insert_instruction(bi, load_count, &load_instruction);

                            load_count += 1;

                            let inserted_result =
                                inserted_load_instruction.try_get_result().unwrap();

                            // Update uses of Phi instruction result
                            for user_instr in &mut new_function.basic_blocks[bi].instrs {
                                // TODO: refactor
                                for operand in user_instr
                                    .get_operands_mut()
                                    .iter_mut()
                                    .map(|op| match op {
                                        Operand::LocalOperand { name, .. } => {
                                            if name == instr.try_get_result().unwrap() {
                                                Some(op)
                                            } else {
                                                None
                                            }
                                        }
                                        _ => None,
                                    })
                                    .filter(|op| op.is_some())
                                {
                                    if let Some(Operand::LocalOperand { name, .. }) = operand {
                                        *name = inserted_result.clone()
                                    }
                                }
                            }
                        } else if let Operand::LocalOperand { name, .. } = instr
                            .get_operands()
                            .iter()
                            .find(|op| matches!(op, Operand::LocalOperand { .. }))
                            .unwrap()
                        {
                            let load_instruction = get_read_instruction(
                                module,
                                &Name::Name(Box::new(format!(
                                    "{}{}",
                                    GLOBAL_REGISTER_NAME_PREFIX,
                                    *global_reg_refs.get(name).unwrap()
                                ))),
                            );

                            let inserted_load_instruction =
                                new_function.insert_instruction(bi, load_count, &load_instruction);

                            load_count += 1;

                            let inserted_result =
                                inserted_load_instruction.try_get_result().unwrap();

                            // Update uses of Phi instruction result
                            for user_instr in &mut new_function.basic_blocks[bi].instrs {
                                // TODO: refactor
                                for operand in user_instr
                                    .get_operands_mut()
                                    .iter_mut()
                                    .map(|op| match op {
                                        Operand::LocalOperand { name, .. } => {
                                            if name == instr.try_get_result().unwrap() {
                                                Some(op)
                                            } else {
                                                None
                                            }
                                        }
                                        _ => None,
                                    })
                                    .filter(|op| op.is_some())
                                {
                                    if let Some(Operand::LocalOperand { name, .. }) = operand {
                                        *name = inserted_result.clone()
                                    }
                                }
                            }
                        }
                    }
                    Instruction::Load(instruction::Load { address, .. }) => {
                        if let Operand::LocalOperand { name, ty } = address {
                            let mut operands_mut = new_function.basic_blocks[bi].instrs
                                [ii + load_count]
                                .get_operands_mut();
                            *operands_mut[0] = Operand::ConstantOperand(ConstantRef::new(
                                Constant::GlobalReference {
                                    name: Name::Name(Box::new(format!(
                                        "{}{}",
                                        LOAD_NAME_PREFIX,
                                        global_reg_refs.get(name).unwrap(),
                                    ))),
                                    ty: ty.clone(),
                                },
                            ));
                        }
                    }
                    _ => {
                        for (oi, operand) in instr.get_operands().iter().enumerate() {
                            if let Operand::LocalOperand { name, .. } = operand {
                                if !name.is_defined_in(basic_block) {
                                    let load_instruction = get_read_instruction(
                                        module,
                                        &Name::Name(Box::new(format!(
                                            "{}{}",
                                            GLOBAL_REGISTER_NAME_PREFIX,
                                            *global_reg_refs.get(&name).unwrap()
                                        ))),
                                    );

                                    let inserted_load_instruction = new_function
                                        .insert_instruction(bi, load_count, &load_instruction);

                                    load_count += 1;

                                    let mut operands_mut = new_function.basic_blocks[bi].instrs
                                        [ii + load_count]
                                        .get_operands_mut();
                                    let operand_mut = &mut operands_mut[oi];

                                    // Replace the operand with the newly inserted result
                                    if let Operand::LocalOperand { name, .. } = operand_mut {
                                        *name = inserted_load_instruction
                                            .try_get_result()
                                            .unwrap()
                                            .clone();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        new_function
    }
}

fn remove_next_phi_instruction(function: &Function) -> Option<Function> {
    let mut new_function = function.clone();

    for (bi, basic_block) in function.basic_blocks.iter().enumerate() {
        for (ii, instruction) in basic_block.instrs.iter().enumerate() {
            if let Instruction::Phi(_) = instruction {
                new_function.remove_instruction(bi, ii, None);
                return Some(new_function);
            }
        }
    }

    None
}

trait RemovePhiInstructions {
    fn remove_phi_instructions(&self) -> Function;
}

impl RemovePhiInstructions for Function {
    fn remove_phi_instructions(&self) -> Function {
        let mut new_function = self.clone();

        while let Some(fu) = remove_next_phi_instruction(&new_function) {
            new_function = fu;
        }

        new_function
    }
}

trait ShouldLoop {
    fn should_loop(&self) -> bool;
}

impl ShouldLoop for BasicBlock {
    fn should_loop(&self) -> bool {
        self.can_branch_to(self)
    }
}

trait PrepareLoopXMode {
    fn prepare_loop_xmode(&self) -> Function;
}

impl PrepareLoopXMode for Function {
    fn prepare_loop_xmode(&self) -> Function {
        let mut new_function = self.clone();

        let mut removed_cnt = 0;
        for (bi, basic_block) in self.basic_blocks.iter().enumerate() {
            for (ii, instr) in basic_block.instrs.iter().enumerate() {
                if basic_block.should_loop() {
                    // FIXME: only remove Store instructions that are used over iterations, the
                    // ones that are also READ
                    if let Instruction::Store(_) = instr {
                        new_function.remove_instruction(bi, ii - removed_cnt, None);
                        removed_cnt += 1;
                    }
                }
            }
        }

        new_function
    }
}
