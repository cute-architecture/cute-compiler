/*
 * Copyright © Romain Fouquet, 2020
 *
 * This file is part of the CUTE compiler.
 *
 * The CUTE compiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The CUTE compiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with The CUTE compiler.  If not, see <https://www.gnu.org/licenses/>.
 */

use llvm_ir::module::GlobalVariable;
use llvm_ir::{BasicBlock, Constant, Function, Instruction, Terminator};

use cuteisa::{BLOCK_HEADER_SIZE, BYTE_SIZE, INSTRUCTION_SIZE, SUBBLOCK_DESCRIPTOR_SIZE};

pub trait GetCodeSizeBytes {
    fn get_code_size_bytes(&self) -> u32;
}

impl GetCodeSizeBytes for Function {
    fn get_code_size_bytes(&self) -> u32 {
        self.basic_blocks
            .iter()
            .map(|b| b.get_code_size_bytes())
            .sum()
    }
}

impl GetCodeSizeBytes for BasicBlock {
    fn get_code_size_bytes(&self) -> u32 {
        BLOCK_HEADER_SIZE as u32 / BYTE_SIZE as u32
            + self
                .instrs
                .iter()
                .map(|i| i.get_code_size_bytes())
                .sum::<u32>()
            + self.term.get_code_size_bytes()
            + (match self.term {
                Terminator::Br(_) => 1,     // 1 subblock
                Terminator::CondBr(_) => 2, // 2 subblocks
                Terminator::Ret(_) => 0,    // FIXME: depends on the instructions inserted
                _ => unimplemented!(),
            }) * (SUBBLOCK_DESCRIPTOR_SIZE as u32 / BYTE_SIZE as u32)
    }
}

impl GetCodeSizeBytes for Instruction {
    fn get_code_size_bytes(&self) -> u32 {
        INSTRUCTION_SIZE as u32 / BYTE_SIZE as u32
    }
}

impl GetCodeSizeBytes for Terminator {
    fn get_code_size_bytes(&self) -> u32 {
        INSTRUCTION_SIZE as u32 / BYTE_SIZE as u32
    }
}

impl GetCodeSizeBytes for GlobalVariable {
    fn get_code_size_bytes(&self) -> u32 {
        self.initializer.as_ref().unwrap().get_code_size_bytes()
    }
}

impl GetCodeSizeBytes for Constant {
    fn get_code_size_bytes(&self) -> u32 {
        match self {
            Constant::Int { bits, .. } => bits / BYTE_SIZE as u32,
            // FIXME: this depends on alignment
            Constant::Array { elements, .. } => {
                elements.iter().map(|e| e.get_code_size_bytes()).sum()
            }
            // FIXME: this depends on alignment
            Constant::Struct { values, .. } => values.iter().map(|e| e.get_code_size_bytes()).sum(),
            _ => unimplemented!(),
        }
    }
}
