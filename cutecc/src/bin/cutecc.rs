/*
 * Copyright © Romain Fouquet, 2020
 *
 * This file is part of the CUTE compiler.
 *
 * The CUTE compiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The CUTE compiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with The CUTE compiler.  If not, see <https://www.gnu.org/licenses/>.
 */

use std::fs;
use std::io::Write;
use std::path::PathBuf;
use std::process::Command;

use clap::Parser;
use llvm_ir::Module;

use cutecc::print_asm::{PrintAsm, PrintAsmOptions};

#[derive(Parser)]
#[clap(about = "C compiler for the CUTE architecture.")]
struct Args {
    /// Input file
    #[clap(parse(from_os_str))]
    input_file: PathBuf,

    /// Generate an output file
    #[clap(short = 'o', parse(from_os_str))]
    bin_file: Option<PathBuf>,

    /// Generate an assembly file
    #[clap(short = 'S', parse(from_os_str))]
    asm_file: Option<PathBuf>,

    /// Generate a hex file
    #[clap(long = "hex", parse(from_os_str))]
    hex_file: Option<PathBuf>,

    /// Enable block SPECulative execution
    #[clap(long = "spec")]
    allow_block_spec: bool,

    /// Enable block LOOPing
    #[clap(long = "loop")]
    allow_block_loop: bool,
}

fn main() {
    let args = Args::parse();

    let ll_file = args
        .asm_file
        .as_ref()
        .unwrap()
        .with_extension("ll")
        .to_string_lossy()
        .to_string();

    Command::new("clang")
        .args([
            "--target=arm",
            "-emit-llvm",
            "-Xclang",
            "-disable-O0-optnone",
            "-c",
            &args.input_file.to_string_lossy().to_string(),
            "-o",
            &ll_file,
        ])
        .status()
        .expect("could not generate LLVM IR ll file");

    Command::new("clang")
        .args([
            "--target=arm",
            "-emit-llvm",
            "-Xclang",
            "-disable-O0-optnone",
            "-c",
            &args.input_file.to_string_lossy().to_string(),
            "-o",
            &args
                .asm_file
                .as_ref()
                .unwrap()
                .with_extension("bc")
                .to_string_lossy()
                .to_string(),
        ])
        .status()
        .expect("could not generate bitcode file");

    let ll_opt_file = args
        .asm_file
        .as_ref()
        .unwrap()
        .with_file_name(
            args.asm_file
                .as_ref()
                .unwrap()
                .file_name()
                .unwrap()
                .to_string_lossy()
                .to_string()
                + "_opt",
        )
        .with_extension("ll")
        .to_string_lossy()
        .to_string();

    Command::new("opt")
        .args([
            "--lowerswitch",
            "--licm",
            "--mem2reg",
            "--ipsccp",
            "--dse",
            "--loop-rotate",
            "--partial-inliner",
            "--inline",
            "--sink",
            "--memcpyopt",
            "--simplifycfg",
            "--instcombine",
            "--aggressive-instcombine",
            "--mem2reg",
            "-S",
            &ll_file,
            "-o",
            &ll_opt_file,
        ])
        .status()
        .expect("could not generate optimized LLVM IR ll file");

    let bc_opt_file = args
        .asm_file
        .as_ref()
        .unwrap()
        .with_file_name(
            args.asm_file
                .as_ref()
                .unwrap()
                .file_name()
                .unwrap()
                .to_string_lossy()
                .to_string()
                + "_opt",
        )
        .with_extension("bc")
        .to_string_lossy()
        .to_string();

    Command::new("llvm-as")
        .args([&ll_opt_file, "-o", &bc_opt_file])
        .status()
        .expect("could not generate optimized bitcode file");

    let module = Module::from_bc_path(bc_opt_file).expect("cannot parse module");

    let print_asm_options = PrintAsmOptions {
        allow_block_spec: args.allow_block_spec,
        allow_block_loop: args.allow_block_loop,
    };

    let function_main_asm = module.print_asm(&print_asm_options);
    println!("{}", function_main_asm);
    let program = cuteas::parse_assembly(&function_main_asm).unwrap_or_else(|e| panic!("{}", e));

    let program_code = cuteas::generate_program_code(&program).unwrap();

    // FIXME: handle params in a better way
    if let Some(bin_file) = args.bin_file {
        let mut bin_file = fs::File::create(bin_file).expect("cannot open file to write");
        bin_file
            .write_all(&program_code.0)
            .expect("cannot write to file");
    }

    if let Some(asm_file) = args.asm_file {
        fs::write(asm_file, function_main_asm).expect("cannot write to file");
    }

    if let Some(hex_file) = args.hex_file {
        fs::write(hex_file, &program_code.1).expect("cannot write to write");
    }
}
