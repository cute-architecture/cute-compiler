/*
 * Copyright © Romain Fouquet, 2020
 *
 * This file is part of the CUTE compiler.
 *
 * The CUTE compiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The CUTE compiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with The CUTE compiler.  If not, see <https://www.gnu.org/licenses/>.
 */

use std::collections::HashMap;
use std::fs;
use std::iter;
use std::str::FromStr;

use llvm_ir::instruction::{self, BinaryOp};
use llvm_ir::module::GlobalVariable;
use llvm_ir::predicates::IntPredicate;
use llvm_ir::terminator;
use llvm_ir::types::Type;
use llvm_ir::{BasicBlock, Constant, Function, Instruction, Module, Name, Operand, Terminator};

use cuteisa::{can_be_lower_immediate_value, can_be_upper_immediate_value};
use cuteisa::{BYTE_SIZE, CUTE_UPPER_IMM_SHIFT, MAX_MAX_EXEC_DEPTH};

use super::get_code_size_bytes::GetCodeSizeBytes;
use super::legalize::Legalize;
use super::llvm_ir_utils::get_name_str::GetNameStr;
use super::llvm_ir_utils::get_operands::GetOperands;
use super::llvm_ir_utils::get_predecessors::GetPredecessors;
use super::llvm_ir_utils::print_ir::PrintIR;
use super::llvm_ir_utils::uses::Uses;
use super::InsertReadsWrites;
use super::PrepareLoopXMode;
use super::RemovePhiInstructions;
use super::ShouldLoop;
use super::{GLOBAL_REGISTER_NAME_PREFIX, LOAD_NAME_PREFIX};

const CUTE_INSTR_MNEMONIC_WIDTH: usize = 7;
const INSTR_INDENT: usize = 2;

pub struct PrintAsmOptions {
    pub allow_block_spec: bool,
    pub allow_block_loop: bool,
}

fn format_instruction_asm(instr_mnemonic: &str, instr_params: &str) -> String {
    format!(
        "{:<indent$}{:<width$} {}",
        "",
        instr_mnemonic,
        instr_params,
        indent = INSTR_INDENT,
        width = CUTE_INSTR_MNEMONIC_WIDTH
    )
}

pub trait PrintAsm {
    fn print_asm(&self, print_asm_options: &PrintAsmOptions) -> String;
}

trait PrintAsmInFunction {
    fn print_asm(
        &self,
        function: &Function,
        global_reg_alloc: &HashMap<Name, usize>,
        print_asm_options: &PrintAsmOptions,
    ) -> String;
}

trait PrintAsmTerminator {
    fn print_asm(
        &self,
        block: &BasicBlock,
        function: &Function,
        local_reg_alloc: &HashMap<Name, usize>,
        print_asm_options: &PrintAsmOptions,
    ) -> String;
}

trait PrintAsmRegAlloc {
    fn print_asm(
        &self,
        local_reg_alloc: &HashMap<Name, usize>,
        global_reg_alloc: &HashMap<Name, usize>,
    ) -> String;
}

macro_rules! print_asm_binary_instruction {
    ($instruction: path, $instruction_mnemomic_asm: literal) => {
        impl PrintAsmRegAlloc for $instruction {
            fn print_asm(
                &self,
                local_reg_alloc: &HashMap<Name, usize>,
                _global_reg_alloc: &HashMap<Name, usize>,
            ) -> String {
                let dest_reg = local_reg_alloc.get(&self.dest).unwrap();

                match self.get_operand1() {
                    Operand::LocalOperand { name, .. } => {
                        let source_reg_a = local_reg_alloc.get(&name).unwrap();
                        match &self.operand0 {
                            Operand::LocalOperand { name, .. } => {
                                let source_reg_b = local_reg_alloc.get(&name).unwrap();
                                format_instruction_asm(
                                    $instruction_mnemomic_asm,
                                    &format!("L{}, L{}, L{}", dest_reg, source_reg_b, source_reg_a),
                                )
                            }
                            _ => unreachable!(),
                        }
                    }
                    Operand::ConstantOperand(cref) => match cref.as_ref() {
                        Constant::Int { value, .. } => match &self.operand0 {
                            Operand::LocalOperand { name, .. } => {
                                let source_reg = local_reg_alloc.get(&name).unwrap();
                                if can_be_lower_immediate_value(*value) {
                                    format_instruction_asm(
                                        &($instruction_mnemomic_asm.to_string() + "I"),
                                        &format!("L{}, L{}, ${:x}", dest_reg, source_reg, value),
                                    )
                                } else {
                                    if can_be_upper_immediate_value(*value) {
                                        format_instruction_asm(
                                            &($instruction_mnemomic_asm.to_string() + "UI"),
                                            &format!(
                                                "L{}, L{}, ${:x}",
                                                dest_reg, source_reg, value
                                            ),
                                        )
                                    } else {
                                        unreachable!();
                                    }
                                }
                            }
                            Operand::ConstantOperand(cref) => match cref.as_ref() {
                                Constant::Int { value: 0, .. } => {
                                    if can_be_lower_immediate_value(*value) {
                                        format_instruction_asm(
                                            &($instruction_mnemomic_asm.to_string() + "I"),
                                            &format!("L{}, L0, ${:x}", dest_reg, value),
                                        )
                                    } else {
                                        if can_be_upper_immediate_value(*value) {
                                            format_instruction_asm(
                                                &($instruction_mnemomic_asm.to_string() + "UI"),
                                                &format!(
                                                    "L{}, L0, ${:x}",
                                                    dest_reg,
                                                    value >> CUTE_UPPER_IMM_SHIFT
                                                ),
                                            )
                                        } else {
                                            unreachable!();
                                        }
                                    }
                                }
                                _ => unreachable!(),
                            },
                            _ => {
                                dbg!(&self);
                                unreachable!()
                            }
                        },
                        _ => unreachable!(),
                    },
                    _ => unimplemented!(),
                }
            }
        }
    };
}

print_asm_binary_instruction!(instruction::Add, "ADD");
print_asm_binary_instruction!(instruction::And, "AND");
print_asm_binary_instruction!(instruction::Xor, "XOR");
print_asm_binary_instruction!(instruction::Shl, "LSL");
// TODO: add other binary instructions

impl PrintAsmRegAlloc for instruction::Load {
    fn print_asm(
        &self,
        local_reg_alloc: &HashMap<Name, usize>,
        global_reg_alloc: &HashMap<Name, usize>,
    ) -> String {
        match self.address.as_constant() {
            Some(Constant::GlobalReference {
                name: name @ Name::Name(greg),
                ..
            }) if greg.starts_with(GLOBAL_REGISTER_NAME_PREFIX) => {
                let dest_reg = local_reg_alloc.get(&self.dest).unwrap();
                let source_reg = global_reg_alloc.get(name).unwrap();
                format_instruction_asm("READ", &format!("L{}, R{}", dest_reg, source_reg))
            }
            Some(Constant::GlobalReference {
                name: Name::Name(ldr),
                ..
            }) if ldr.starts_with(LOAD_NAME_PREFIX) => {
                let dest_reg = local_reg_alloc.get(&self.dest).unwrap();
                let addr_reg = global_reg_alloc.get(&get_ldr_global_reg_name(ldr)).unwrap();
                // TODO: use the box pattern when becomes stable
                let word_size = match self.address.as_constant() {
                    Some(Constant::GlobalReference { ty, .. }) => match ty.as_ref() {
                        Type::PointerType {
                            ref pointee_type, ..
                        } => match **pointee_type {
                            Type::IntegerType { bits } => bits,
                            _ => unimplemented!(),
                        },
                        _ => unreachable!(),
                    },
                    _ => unimplemented!(),
                };
                format_instruction_asm(
                    if word_size == BYTE_SIZE as u32 {
                        "LDRB"
                    } else {
                        "LDR"
                    },
                    &format!("L{}, [R{}]", dest_reg, addr_reg),
                )
            }
            _ => unimplemented!(),
        }
    }
}

impl PrintAsmRegAlloc for instruction::Store {
    fn print_asm(
        &self,
        local_reg_alloc: &HashMap<Name, usize>,
        global_reg_alloc: &HashMap<Name, usize>,
    ) -> String {
        // TODO: refactor the matches
        match &self.address {
            Operand::ConstantOperand(cref) => match cref.as_ref() {
                Constant::GlobalReference { name, .. } => {
                    let dest_reg = global_reg_alloc.get(name).unwrap();
                    match &self.value {
                        Operand::LocalOperand { name, .. } => {
                            // FIXME: fix name shadowing
                            let source_reg = local_reg_alloc.get(name).unwrap();
                            format_instruction_asm(
                                "WRITE",
                                &format!("R{}, L{}", dest_reg, source_reg),
                            )
                        }
                        Operand::ConstantOperand(cref) => match cref.as_ref() {
                            Constant::Int { value, .. } => format_instruction_asm(
                                "WRITEI",
                                &format!("R{}, ${:x}", dest_reg, value),
                            ),
                            _ => unreachable!(),
                        },
                        _ => unreachable!(),
                    }
                }
                _ => unreachable!(),
            },
            Operand::LocalOperand {
                name: addr_name, ..
            } => {
                match &self.value {
                    Operand::LocalOperand {
                        name: value_name, ..
                    } => {
                        let addr_reg = local_reg_alloc.get(addr_name).unwrap();
                        let value_reg = local_reg_alloc.get(value_name).unwrap();
                        format_instruction_asm("STR", &format!("[L{}], L{}", addr_reg, value_reg,))
                    }
                    Operand::ConstantOperand(cref) => match cref.as_ref() {
                        Constant::Int { value, .. } => match &self.address {
                            Operand::LocalOperand { name, .. } => {
                                // FIXME: handle upper immediate value
                                let addr_reg = local_reg_alloc.get(name).unwrap();
                                format_instruction_asm(
                                    "STRI",
                                    &format!("[L{}], ${:x}", addr_reg, value,),
                                )
                            }
                            _ => unreachable!(),
                        },
                        _ => unreachable!(),
                    },
                    _ => unimplemented!(),
                }
            }
            _ => unimplemented!(),
        }
    }
}

trait ToCuteString {
    fn to_cute_string(&self) -> &'static str;
}

impl ToCuteString for IntPredicate {
    fn to_cute_string(&self) -> &'static str {
        match self {
            Self::EQ => "EQ",
            Self::NE => "NE",
            Self::UGT => "UGT",
            Self::UGE => "UGE",
            Self::ULT => "ULT",
            Self::ULE => "ULE",
            Self::SGT => "SGT",
            Self::SGE => "SGE",
            Self::SLT => "SLT",
            Self::SLE => "SLE",
        }
    }
}

impl PrintAsmRegAlloc for instruction::ICmp {
    fn print_asm(
        &self,
        local_reg_alloc: &HashMap<Name, usize>,
        _global_reg_alloc: &HashMap<Name, usize>,
    ) -> String {
        let dest_reg = local_reg_alloc.get(&self.dest).unwrap();

        match &self.operand1 {
            Operand::LocalOperand { name: name_b, .. } => match &self.operand0 {
                Operand::LocalOperand { name: name_a, .. } => {
                    let source_b_reg = local_reg_alloc.get(name_b).unwrap();
                    let source_a_reg = local_reg_alloc.get(name_a).unwrap();
                    format_instruction_asm(
                        "CMP",
                        &format!(
                            "L{}, L{}, {}, L{}",
                            dest_reg,
                            source_a_reg,
                            self.predicate.to_cute_string(),
                            source_b_reg,
                        ),
                    )
                }
                _ => unreachable!(),
            },
            Operand::ConstantOperand(cref) => match cref.as_ref() {
                Constant::Int { value, .. } => match &self.operand0 {
                    Operand::LocalOperand { name, .. } => {
                        // FIXME: handle upper immediate value?
                        let source_reg = local_reg_alloc.get(name).unwrap();
                        format_instruction_asm(
                            "CMPI",
                            &format!(
                                "L{}, L{}, {}, ${:x}",
                                dest_reg,
                                source_reg,
                                self.predicate.to_cute_string(),
                                value
                            ),
                        )
                    }
                    _ => {
                        dbg!(&self);
                        unreachable!()
                    }
                },
                _ => unreachable!(),
            },
            _ => unimplemented!(),
        }
    }
}

impl PrintAsmRegAlloc for instruction::BitCast {
    fn print_asm(
        &self,
        _local_reg_alloc: &HashMap<Name, usize>,
        _global_reg_alloc: &HashMap<Name, usize>,
    ) -> String {
        match self {
            instruction::BitCast {
                operand: Operand::ConstantOperand(cref),
                to_type,
                dest: Name::Number(0),
                ..
            } => match cref.as_ref() {
                Constant::Int { bits: 32, value: 0 } => match to_type.as_ref() {
                    Type::IntegerType { bits: 32 } => {
                        format!("{:<indent$}NOP", "", indent = INSTR_INDENT)
                    }
                    _ => unreachable!(),
                },
                _ => unreachable!(),
            },
            _ => unimplemented!(),
        }
    }
}

impl PrintAsmRegAlloc for Instruction {
    fn print_asm(
        &self,
        local_reg_alloc: &HashMap<Name, usize>,
        global_reg_alloc: &HashMap<Name, usize>,
    ) -> String {
        match self {
            // FIXME: implement print_asm for other operations, try to leverage BinaryOp
            Instruction::Add(i) => i.print_asm(local_reg_alloc, global_reg_alloc),
            Instruction::And(i) => i.print_asm(local_reg_alloc, global_reg_alloc),
            Instruction::Xor(i) => i.print_asm(local_reg_alloc, global_reg_alloc),
            Instruction::Shl(i) => i.print_asm(local_reg_alloc, global_reg_alloc),
            Instruction::Load(i) => i.print_asm(local_reg_alloc, global_reg_alloc),
            Instruction::Store(i) => i.print_asm(local_reg_alloc, global_reg_alloc),
            Instruction::ICmp(i) => i.print_asm(local_reg_alloc, global_reg_alloc),
            Instruction::Select(i) =>
            // FIXME
            {
                format!("; FIXME {}", i)
            }
            Instruction::BitCast(i) => i.print_asm(local_reg_alloc, global_reg_alloc),
            Instruction::Alloca(_) => String::new(), // FIXME
            // Instruction::GetElementPtr(_) => String::new(), // FIXME
            Instruction::Trunc(i) =>
            // FIXME
            {
                format!("; FIXME {}", i)
            }
            Instruction::Call(i) =>
            // FIXME
            {
                format!("; FIXME {}", i)
            }
            _ => {
                dbg!(&self);
                unimplemented!()
            }
        }
    }
}

fn get_block_name_asm(function_name: &str, block_name: &Name) -> String {
    format!(
        "{}${}",
        function_name,
        match block_name {
            Name::Number(number) => number.to_string(),
            Name::Name(name) => name.to_string(),
        },
    )
}

fn get_block_xmode(
    block: &BasicBlock,
    dest_name: &Name,
    max_exec_depth: u8,
    print_asm_options: &PrintAsmOptions,
) -> &'static str {
    if print_asm_options.allow_block_loop && &block.name == dest_name && block.should_loop() {
        return "LOOP";
    }

    if print_asm_options.allow_block_spec && max_exec_depth > 0 {
        return "SPEC";
    }

    "NOSPEC"
}

impl PrintAsmTerminator for Terminator {
    fn print_asm(
        &self,
        block: &BasicBlock,
        function: &Function,
        local_reg_alloc: &HashMap<Name, usize>,
        print_asm_options: &PrintAsmOptions,
    ) -> String {
        match self {
            Terminator::Br(terminator::Br { dest, .. }) => {
                let dest_block = function
                    .basic_blocks
                    .iter()
                    .find(|b| &b.name == dest)
                    .unwrap();
                let max_exec_depth = get_max_exec_depth(dest_block, function);
                format_instruction_asm(
                    "BRI",
                    &format!(
                        "({}, {}, 0)",
                        get_block_name_asm(&function.name, dest),
                        get_block_xmode(block, dest, max_exec_depth, print_asm_options),
                    ),
                )
            }
            Terminator::CondBr(terminator::CondBr {
                false_dest,
                true_dest,
                condition: Operand::LocalOperand { name, .. },
                ..
            }) => {
                let false_dest_block = function
                    .basic_blocks
                    .iter()
                    .find(|b| &b.name == false_dest)
                    .unwrap();
                let true_dest_block = function
                    .basic_blocks
                    .iter()
                    .find(|b| &b.name == true_dest)
                    .unwrap();
                let false_max_exec_depth = get_max_exec_depth(false_dest_block, function);
                let true_max_exec_depth = get_max_exec_depth(true_dest_block, function);
                format_instruction_asm(
                    "BR",
                    &format!(
                        "({}, {}, {}), L{}, ({}, {}, {})",
                        get_block_name_asm(&function.name, false_dest),
                        get_block_xmode(block, false_dest, false_max_exec_depth, print_asm_options),
                        0,                                  // FIXME
                        local_reg_alloc.get(name).unwrap(), // FIXME: use condition
                        get_block_name_asm(&function.name, true_dest),
                        get_block_xmode(block, true_dest, true_max_exec_depth, print_asm_options),
                        1, // FIXME
                    ),
                )
            }
            Terminator::Ret(ret) =>
            // FIXME
            {
                format!("; FIXME {}", ret)
            }
            _ => {
                dbg!(&self);
                unimplemented!()
            }
        }
    }
}

fn get_ldr_global_reg_name(nu: &str) -> Name {
    Name::Name(Box::new(format!(
        "{}{}",
        GLOBAL_REGISTER_NAME_PREFIX,
        usize::from_str(&nu.chars().skip(LOAD_NAME_PREFIX.chars().count()).collect::<String>()).unwrap(),
    )))
}

fn get_consumed_global_regs(block: &BasicBlock) -> Vec<Name> {
    block
        .instrs
        .iter()
        .filter_map(|i| match i {
            Instruction::Load(instruction::Load {
                address: Operand::ConstantOperand(cref),
                ..
            }) => match cref.as_ref() {
                Constant::GlobalReference { name, .. } => match name {
                    Name::Name(greg) if greg.starts_with(GLOBAL_REGISTER_NAME_PREFIX) => {
                        Some(name.clone())
                    }
                    Name::Name(nu) => dbg!(Some(get_ldr_global_reg_name(nu))),
                    _ => unimplemented!(),
                },
                _ => unreachable!(),
            },
            _ => None,
        })
        .collect::<Vec<_>>()
}

fn get_produced_global_regs(block: &BasicBlock) -> Vec<Name> {
    // FIXME
    block
        .instrs
        .iter()
        .filter_map(|i| match i {
            Instruction::Store(instruction::Store {
                address: Operand::ConstantOperand(cref),
                ..
            }) => match cref.as_ref() {
                Constant::GlobalReference { name, .. } => Some(name.clone()),
                _ => unreachable!(),
            },
            _ => None,
        })
        .collect::<Vec<_>>()
}

fn get_max_exec_depth(block: &BasicBlock, function: &Function) -> u8 {
    let accessed_global_regs = get_consumed_global_regs(block);

    if accessed_global_regs.is_empty() {
        return MAX_MAX_EXEC_DEPTH;
    }

    let has_ldr_instructions = block.instrs.iter().any(|i| match i {
        Instruction::Load(instruction::Load {
            address: Operand::ConstantOperand(cref),
            ..
        }) => matches!(
            cref.as_ref(),
            Constant::GlobalReference {
                name: Name::Name(_),
                ..
            }
        ),
        _ => false,
    });

    let mut current_predecessors = block.get_predecessors(function);
    let mut current_depth = 0;

    loop {
        // If this block is in the current_predecessors
        if let Some(index) = current_predecessors.iter().position(|&b| b == block) {
            current_predecessors.remove(index);
        }

        if current_predecessors.is_empty() {
            break;
        }

        // If any of the current_predecessors produces a value stored in a global register
        // read by this block
        if current_predecessors.iter().any(|b| {
            let produced_global_regs = get_produced_global_regs(b);
            accessed_global_regs
                .iter()
                .any(|n| produced_global_regs.iter().any(|nn| nn == n))
        }) {
            break;
        }

        // TODO: ease this condition: it is safe to spec exec a block with LDR instructions when the
        // address is known at compile time and is not written to by predecessor blocks
        // If this block has at least one LDR instruction and any of the current_predecessors
        // stores a value in memory
        if has_ldr_instructions
            && current_predecessors.iter().any(|b| {
                b.instrs.iter().any(|i| {
                    matches!(
                        i,
                        Instruction::Store(instruction::Store {
                            address: Operand::LocalOperand { .. },
                            ..
                        })
                    )
                })
            })
        {
            break;
        }

        current_predecessors = current_predecessors
            .iter()
            .flat_map(|b| b.get_predecessors(function))
            .collect::<Vec<_>>();
        current_predecessors.sort_by_key(|b| b.name.clone());
        current_predecessors.dedup();

        current_depth += 1;
    }

    current_depth
}

impl PrintAsmInFunction for BasicBlock {
    fn print_asm(
        &self,
        function: &Function,
        global_reg_alloc: &HashMap<Name, usize>,
        print_asm_options: &PrintAsmOptions,
    ) -> String {
        let mut local_reg_alloc = HashMap::new();
        let mut free_reg = vec![true; cuteisa::LOCAL_REG_NB as usize];

        if self.instrs.len() > cuteisa::MAX_BODY_LENGTH {
            panic!("Too many instructions in this block!");
        }

        // TODO: improve this
        free_reg[0] = false; // L0 is actually reserved, to hold value 0

        for (ii, instruction) in self.instrs.iter().enumerate() {
            // Free registers when possible
            for op in instruction.get_operands() {
                if let Operand::LocalOperand { name, .. } = op {
                    let is_loop_read = self.instrs.iter().any(|i| match i {
                        Instruction::Load(instruction::Load { dest, .. }) => dest == name,
                        _ => false,
                    });

                    // Skip freeing the local register if it is in a loop-able block and is not
                    // used over iterations
                    if self.should_loop() && !is_loop_read {
                        continue;
                    }

                    dbg!(&name);
                    let (_, reg_index) = local_reg_alloc.iter().find(|(n, _)| *n == name).unwrap();

                    free_reg[*reg_index] = !self.instrs.iter().skip(ii + 1).any(|i| i.uses(name));
                }
            }

            // If the instruction produces a result, allocate a local register to store it
            if let Some(name) = instruction.try_get_result() {
                let allocated_reg_index = free_reg
                    .iter()
                    .position(|&r| r)
                    .expect("not enough local registers");
                local_reg_alloc.insert(name.clone(), allocated_reg_index);
                free_reg[allocated_reg_index] = false;
            }
        }

        let block_max_exec_depth = get_max_exec_depth(self, function);
        let block_label_line = format!(
            "{}: < {}",
            get_block_name_asm(&function.name, &self.name),
            block_max_exec_depth,
        );

        iter::once(block_label_line)
            .chain(
                self.instrs
                    .iter()
                    .map(|i| i.print_asm(&local_reg_alloc, global_reg_alloc)),
            )
            .chain(iter::once(self.term.print_asm(
                self,
                function,
                &local_reg_alloc,
                print_asm_options,
            )))
            .collect::<Vec<String>>()
            .join("\n")
    }
}

impl PrintAsm for Function {
    fn print_asm(&self, print_asm_options: &PrintAsmOptions) -> String {
        let mut global_reg_alloc = HashMap::new();
        let mut free_reg = vec![true; cuteisa::GLOBAL_REG_NB as usize];

        // FIXME: free registers when last use, when possible

        for instruction in self.basic_blocks.iter().flat_map(|b| &b.instrs) {
            for operand in instruction.get_operands() {
                if let Some(Constant::GlobalReference { name, .. }) = operand.as_constant() {
                    if !global_reg_alloc.contains_key(name) {
                        let allocated_reg_index = free_reg
                            .iter()
                            .position(|&r| r)
                            .expect("not enough global registers");
                        global_reg_alloc.insert(name.clone(), allocated_reg_index);
                        free_reg[allocated_reg_index] = false;
                    }
                }
            }
        }

        // dbg!(&global_reg_alloc);

        self.basic_blocks
            .iter()
            .map(|bb| bb.print_asm(self, &global_reg_alloc, print_asm_options))
            .collect::<Vec<_>>()
            .join("\n\n")
            + "\n"
    }
}

impl PrintAsm for Constant {
    fn print_asm(&self, print_asm_options: &PrintAsmOptions) -> String {
        match self {
            Constant::Int { value, bits } => format_instruction_asm(
                if *bits == BYTE_SIZE as u32 {
                    ".byte"
                } else {
                    ".word"
                },
                &value.to_string(),
            ),
            Constant::Array { elements, .. } => elements
                .iter()
                .map(|e| e.print_asm(print_asm_options))
                .collect::<Vec<_>>()
                .join("\n"),
            _ => unimplemented!(),
        }
    }
}

impl PrintAsm for GlobalVariable {
    fn print_asm(&self, print_asm_options: &PrintAsmOptions) -> String {
        format!(
            "{}:\n\
            {}",
            self.name.get_name_str(),
            self.initializer
                .as_ref()
                .unwrap()
                .print_asm(print_asm_options),
        )
    }
}

impl PrintAsm for Module {
    fn print_asm(&self, print_asm_options: &PrintAsmOptions) -> String {
        let section_rodata = self
            .global_vars
            .iter()
            .map(|gv| {
                format!(
                    "\n\
                    .global {obj}\n\
                    .section .rodata\n\
                    .type {obj}, %object\n\
                    .size {obj}, {}\n\
                    {}\n",
                    2, // FIXME: size
                    gv.print_asm(print_asm_options),
                    obj = gv.name.get_name_str(),
                )
            })
            .collect::<Vec<_>>()
            .join("");

        // TODO: maybe rewrite this with iterators
        let mut rodata_symbol_table = HashMap::new();
        let mut offset = 0;
        for gv in &self.global_vars {
            rodata_symbol_table.insert(gv.name.clone(), offset);
            offset += gv.get_code_size_bytes();
        }

        // FIXME: refactor the way the pseudo-prelinking is done, so that it is not needed to run
        // this twice
        println!("{}", self.print_ir());
        fs::write(self.name.to_string() + ".print_ir.ll", self.print_ir())
            .expect("cannot open file to write");

        let functions = self
            .functions
            .iter()
            .map(|f| {
                let function = f.legalize(self, 0, &rodata_symbol_table);
                println!("{}", function.print_ir());
                let function = function.insert_reads_writes(self);
                // println!("{}", function.print_ir());
                let function = function.remove_phi_instructions();
                // println!("{}", function.print_ir());
                let function = match print_asm_options.allow_block_loop {
                    true => function.prepare_loop_xmode(),
                    false => function,
                };
                // println!("{}", function.print_ir());
                // let function = function.schedule(self);

                let text_size_bytes = function.get_code_size_bytes();

                let function = f.legalize(self, text_size_bytes, &rodata_symbol_table);
                // TODO: some LLVM passes could be applied again after the legalize phase, for instance to
                // move constants initializations outside of LOOPs, which would be READ only at the first
                // iteration
                // println!("{}", function.print_ir());
                let function = function.insert_reads_writes(self);
                // println!("{}", function.print_ir());
                let function = function.remove_phi_instructions();
                // println!("{}", function.print_ir());
                let function = match print_asm_options.allow_block_loop {
                    true => function.prepare_loop_xmode(),
                    false => function,
                };
                // let function = function.schedule(self);
                // println!("{}", function.print_ir());
                let function_asm = function.print_asm(print_asm_options);
                println!("{}", function_asm);
                function_asm
            })
            .collect::<Vec<_>>()
            .join("\n");

        let section_text = format!(
            ".global {func}\n\
             .type {func}, %function\n\
             .text\n\
             {}",
            functions,
            func = "main",
        );

        format!("{}{}", section_text, section_rodata)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use insta::assert_snapshot;
    use pretty_assertions::assert_eq;

    #[test]
    fn test_can_be_lower_immediate_value() {
        assert_eq!(can_be_lower_immediate_value(0x42), true);
        assert_eq!(can_be_lower_immediate_value(0x4242), true);
        assert_eq!(can_be_lower_immediate_value(0x42420), false);
        assert_eq!(can_be_lower_immediate_value(0x42420000), false);
    }

    #[test]
    fn test_can_be_upper_immediate_value() {
        assert_eq!(can_be_upper_immediate_value(0x42), false);
        assert_eq!(can_be_upper_immediate_value(0x4242), false);
        assert_eq!(can_be_upper_immediate_value(0x42420), false);
        assert_eq!(can_be_upper_immediate_value(0x42420000), true);
    }

    #[test]
    fn test_print_asm_icmp() {
        let module =
            Module::from_bc_path("tests/fixtures/uart_led_blink.bc").expect("cannot parse module");

        let mut icmp = instruction::ICmp {
            predicate: IntPredicate::EQ,
            operand0: Operand::LocalOperand {
                name: Name::Number(0),
                ty: module.types.i32(),
            },
            operand1: Operand::LocalOperand {
                name: Name::Number(1),
                ty: module.types.i32(),
            },
            dest: Name::Number(2),
            debugloc: None,
        };

        let mut local_reg_alloc = HashMap::new();
        local_reg_alloc.insert(Name::Number(0), 1);
        local_reg_alloc.insert(Name::Number(1), 2);
        local_reg_alloc.insert(Name::Number(2), 3);

        assert_eq!(
            icmp.print_asm(&local_reg_alloc, &HashMap::new()),
            "  CMP     L3, L1, EQ, L2"
        );
        icmp.predicate = IntPredicate::ULT;
        assert_eq!(
            icmp.print_asm(&local_reg_alloc, &HashMap::new()),
            "  CMP     L3, L1, ULT, L2"
        );
    }

    #[test]
    fn test_print_asm_module() {
        let module =
            Module::from_bc_path("tests/fixtures/uart_led_blink.bc").expect("cannot parse module");
        let print_asm_options = PrintAsmOptions {
            allow_block_spec: true,
            allow_block_loop: false,
        };
        let module_asm = module.print_asm(&print_asm_options);
        assert_snapshot!(module_asm);
    }
}
