/*
 * Copyright © Romain Fouquet, 2020
 *
 * This file is part of the CUTE compiler.
 *
 * The CUTE compiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The CUTE compiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with The CUTE compiler.  If not, see <https://www.gnu.org/licenses/>.
 */

use llvm_ir::{Instruction, Name};

pub trait TryGetResultMut {
    fn try_get_result_mut(&mut self) -> Option<&mut Name>;
}

impl TryGetResultMut for Instruction {
    fn try_get_result_mut(&mut self) -> Option<&mut Name> {
        match self {
            Instruction::Add(i) => Some(&mut i.dest),
            Instruction::Sub(i) => Some(&mut i.dest),
            Instruction::Mul(i) => Some(&mut i.dest),
            Instruction::UDiv(i) => Some(&mut i.dest),
            Instruction::SDiv(i) => Some(&mut i.dest),
            Instruction::URem(i) => Some(&mut i.dest),
            Instruction::SRem(i) => Some(&mut i.dest),
            Instruction::And(i) => Some(&mut i.dest),
            Instruction::Or(i) => Some(&mut i.dest),
            Instruction::Xor(i) => Some(&mut i.dest),
            Instruction::Shl(i) => Some(&mut i.dest),
            Instruction::LShr(i) => Some(&mut i.dest),
            Instruction::AShr(i) => Some(&mut i.dest),
            Instruction::FAdd(i) => Some(&mut i.dest),
            Instruction::FSub(i) => Some(&mut i.dest),
            Instruction::FMul(i) => Some(&mut i.dest),
            Instruction::FDiv(i) => Some(&mut i.dest),
            Instruction::FRem(i) => Some(&mut i.dest),
            Instruction::FNeg(i) => Some(&mut i.dest),
            Instruction::ExtractElement(i) => Some(&mut i.dest),
            Instruction::InsertElement(i) => Some(&mut i.dest),
            Instruction::ShuffleVector(i) => Some(&mut i.dest),
            Instruction::ExtractValue(i) => Some(&mut i.dest),
            Instruction::InsertValue(i) => Some(&mut i.dest),
            Instruction::Alloca(i) => Some(&mut i.dest),
            Instruction::Load(i) => Some(&mut i.dest),
            Instruction::Store(_) => None,
            Instruction::Fence(_) => None,
            Instruction::CmpXchg(i) => Some(&mut i.dest),
            Instruction::AtomicRMW(i) => Some(&mut i.dest),
            Instruction::GetElementPtr(i) => Some(&mut i.dest),
            Instruction::Trunc(i) => Some(&mut i.dest),
            Instruction::ZExt(i) => Some(&mut i.dest),
            Instruction::SExt(i) => Some(&mut i.dest),
            Instruction::FPTrunc(i) => Some(&mut i.dest),
            Instruction::FPExt(i) => Some(&mut i.dest),
            Instruction::FPToUI(i) => Some(&mut i.dest),
            Instruction::FPToSI(i) => Some(&mut i.dest),
            Instruction::UIToFP(i) => Some(&mut i.dest),
            Instruction::SIToFP(i) => Some(&mut i.dest),
            Instruction::PtrToInt(i) => Some(&mut i.dest),
            Instruction::IntToPtr(i) => Some(&mut i.dest),
            Instruction::BitCast(i) => Some(&mut i.dest),
            Instruction::AddrSpaceCast(i) => Some(&mut i.dest),
            Instruction::ICmp(i) => Some(&mut i.dest),
            Instruction::FCmp(i) => Some(&mut i.dest),
            Instruction::Phi(i) => Some(&mut i.dest),
            Instruction::Select(i) => Some(&mut i.dest),
            Instruction::Freeze(i) => Some(&mut i.dest),
            Instruction::Call(i) => i.dest.as_mut(),
            Instruction::VAArg(i) => Some(&mut i.dest),
            Instruction::LandingPad(i) => Some(&mut i.dest),
            Instruction::CatchPad(i) => Some(&mut i.dest),
            Instruction::CleanupPad(i) => Some(&mut i.dest),
        }
    }
}
