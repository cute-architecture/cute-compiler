/*
 * Copyright © Romain Fouquet, 2020
 *
 * This file is part of the CUTE compiler.
 *
 * The CUTE compiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The CUTE compiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with The CUTE compiler.  If not, see <https://www.gnu.org/licenses/>.
 */

use llvm_ir::function::{CallingConvention, FunctionAttribute, Parameter, ParameterAttribute};
use llvm_ir::module::{GlobalAlias, GlobalVariable, Linkage, UnnamedAddr};
use llvm_ir::types::Type;
use llvm_ir::{BasicBlock, Function, Module};

use super::get_name_str::GetNameStr;
use super::get_predecessors::GetPredecessors;

// TODO: find a better solution if possible
pub trait PrependSpaceIfNonEmpty {
    fn prepend_space_if_non_empty(&self) -> String;
}

impl PrependSpaceIfNonEmpty for String {
    fn prepend_space_if_non_empty(&self) -> String {
        match self.is_empty() {
            true => String::new(),
            false => " ".to_string() + self,
        }
    }
}

// TODO: find a better solution if possible
pub trait AppendSpaceIfNonEmpty {
    fn append_space_if_non_empty(&self) -> String;
}

impl AppendSpaceIfNonEmpty for String {
    fn append_space_if_non_empty(&self) -> String {
        match self.is_empty() {
            true => String::new(),
            false => self.to_string() + " ",
        }
    }
}

pub trait SurroundNewlinesIfNotEmpty {
    fn surround_newlines_if_not_empty(&self) -> String;
}

impl SurroundNewlinesIfNotEmpty for String {
    fn surround_newlines_if_not_empty(&self) -> String {
        match self.is_empty() {
            true => String::new(),
            false => "\n".to_string() + self + "\n",
        }
    }
}

pub trait PrintIR {
    fn print_ir(&self) -> String;
}

trait PrintIRFunction {
    fn print_ir(&self, function: &Function) -> String;
}

trait PrintIROptType {
    fn print_ir(&self, print_type: bool) -> String;
}

impl PrintIR for Module {
    fn print_ir(&self) -> String {
        // TODO: add other pieces of metadata
        format!(
            "source_filename = \"{}\"\n\
            target datalayout = \"{}\"\n\
            target triple = \"{}\"\n\
            {}\
            {}\
            {}\
            {}",
            self.source_file_name,
            self.data_layout.layout_str,
            self.target_triple.as_ref().unwrap(),
            // FIXME
            // self.named_struct_types
            //     .iter()
            //     .map(|(string, ty)|
            //         format!(
            //             "%\"{}\" = type {}",
            //             string,
            //             ty.as_ref().unwrap().read().unwrap().print_ir(),
            //         )
            //     )
            //     .collect::<Vec<_>>()
            //     .join("\n")
            //     .surround_newlines_if_not_empty(),
            String::new(),
            self.global_vars
                .iter()
                .map(|gv| gv.print_ir())
                .collect::<Vec<_>>()
                .join("")
                .surround_newlines_if_not_empty(),
            self.global_aliases
                .iter()
                .map(|ga| ga.print_ir())
                .collect::<Vec<_>>()
                .join("")
                .surround_newlines_if_not_empty(),
            self.functions
                .iter()
                .map(|f| f.print_ir())
                .collect::<Vec<_>>()
                .join("\n"),
        )
    }
}

impl PrintIR for GlobalVariable {
    fn print_ir(&self) -> String {
        format!(
            "@{} = {} {}{} {} {}, align {}\n",
            self.name.get_name_str(),
            self.linkage.print_ir(),
            match self.unnamed_addr {
                Some(unnamed_addr) => unnamed_addr.print_ir(),
                _ => String::new(),
            },
            match self.is_constant {
                true => "constant",
                _ => unimplemented!(),
            },
            get_pointee_type(self.ty.as_ref()).unwrap(),
            self.initializer.as_ref().unwrap(),
            self.alignment,
        )
    }
}

impl PrintIR for GlobalAlias {
    fn print_ir(&self) -> String {
        // FIXME
        format!(
            "@{} = {} {}{} @{}\n",
            self.name.get_name_str(),
            self.linkage.print_ir(),
            match self.unnamed_addr {
                Some(unnamed_addr) => unnamed_addr.print_ir(),
                _ => String::new(),
            },
            get_pointee_type(self.ty.as_ref()).unwrap(),
            self.aliasee,
        )
    }
}

impl PrintIR for UnnamedAddr {
    fn print_ir(&self) -> String {
        match self {
            UnnamedAddr::Local => "local_unnamed_addr ".to_string(),
            UnnamedAddr::Global => "unnamed_addr ".to_string(),
        }
    }
}

impl PrintIR for Linkage {
    fn print_ir(&self) -> String {
        (match self {
            Linkage::Private => "private",
            Linkage::External => "", // FIXME
            _ => unimplemented!(),
        })
        .to_string()
    }
}

// TODO: refactor functions returning strings with a trailing space
impl PrintIR for Function {
    fn print_ir(&self) -> String {
        // TODO: add other pieces of metadata
        format!(
            "; Function Attrs: {}\n\
            define {}{}{} @{}({}) {}{{\n{}}}\n",
            self.function_attributes
                .iter()
                .map(|a| a.print_ir())
                .collect::<Vec<_>>()
                .join(" "),
            self.calling_convention.print_ir(),
            self.return_attributes
                .iter()
                .map(|a| a.print_ir())
                .collect::<Vec<_>>()
                .join(" ")
                .append_space_if_non_empty(),
            self.return_type,
            self.name,
            self.parameters
                .iter()
                .map(|p| p.print_ir())
                .collect::<Vec<_>>()
                .join(", "),
            match &self.personality_function {
                Some(personnality_function) => {
                    format!("personality {} ", personnality_function,)
                }
                _ => String::new(),
            },
            self.basic_blocks
                .iter()
                .map(|bb| bb.print_ir(self))
                .collect::<Vec<_>>()
                .join("\n"),
        )
    }
}

impl PrintIR for CallingConvention {
    fn print_ir(&self) -> String {
        match self {
            CallingConvention::C => String::new(),
            CallingConvention::ARM_AAPCS => "arm_aapcscc ".to_string(),
            _ => {
                dbg!(&self);
                unimplemented!()
            }
        }
    }
}

impl PrintIR for Parameter {
    fn print_ir(&self) -> String {
        // FIXME: test attributes
        format!(
            "{}{}",
            self.ty,
            self.attributes
                .iter()
                .map(|pa| pa.print_ir())
                .collect::<Vec<_>>()
                .join(" ")
                .prepend_space_if_non_empty()
        )
    }
}

impl PrintIR for FunctionAttribute {
    fn print_ir(&self) -> String {
        match self {
            FunctionAttribute::NoInline => "noinline".to_string(),
            FunctionAttribute::NoRecurse => "norecurse".to_string(),
            FunctionAttribute::NoReturn => "noreturn".to_string(),
            FunctionAttribute::NoUnwind => "nounwind".to_string(),
            FunctionAttribute::NonLazyBind => "nonlazybind".to_string(),
            FunctionAttribute::ReadNone => "readnone".to_string(),
            FunctionAttribute::UWTable => "uwtable".to_string(),
            FunctionAttribute::StringAttribute { kind, value } => {
                format!("\"{}\"=\"{}\"", kind, value,)
            }
            _ => {
                println!("{:#?}", self);
                unimplemented!()
            }
        }
    }
}

impl PrintIR for ParameterAttribute {
    fn print_ir(&self) -> String {
        match self {
            ParameterAttribute::ZeroExt => "zeroext".to_string(),
            ParameterAttribute::StringAttribute { kind, value } => {
                format!("\"{}\"=\"{}\"", kind, value,)
            }
            _ => {
                println!("{:#?}", self);
                unimplemented!()
            }
        }
    }
}

impl PrintIRFunction for BasicBlock {
    fn print_ir(&self, function: &Function) -> String {
        format!(
            "{}{}{}  {}\n",
            if function.basic_blocks.iter().position(|b| b == self) == Some(0) {
                String::new()
            } else {
                let preds_col = 49;
                let block_predecessors = self.get_predecessors(function);
                let block_name = self.name.get_name_str();

                if block_predecessors.is_empty() {
                    block_name + ":\n"
                } else {
                    // TODO: fix order of preds if possible
                    let preds_str = format!(
                        " ; preds = {}\n",
                        block_predecessors
                            .iter()
                            .map(|b| "%".to_string() + &b.name.get_name_str())
                            .collect::<Vec<_>>()
                            .join(", "),
                    );

                    format!(
                        "{:<preds_col$}{}",
                        block_name + ":",
                        preds_str,
                        preds_col = preds_col,
                    )
                }
            },
            self.instrs
                .iter()
                .map(|i| format!("  {}", i))
                .collect::<Vec<_>>()
                .join("\n"),
            match !self.instrs.is_empty() {
                true => "\n",
                false => "",
            },
            self.term,
        )
    }
}

fn get_pointee_type(pointer_type: &Type) -> Option<Type> {
    match pointer_type {
        Type::PointerType { pointee_type, .. } => Some(pointee_type.as_ref().clone()),
        _ => None,
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use insta::assert_snapshot;

    #[test]
    fn test_print_ir_module() {
        let module =
            Module::from_bc_path("tests/fixtures/uart_led_blink.bc").expect("cannot parse module");
        let module_ir = module.print_ir();
        assert_snapshot!(module_ir);
    }
}
