/*
 * Copyright © Romain Fouquet, 2020
 *
 * This file is part of the CUTE compiler.
 *
 * The CUTE compiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The CUTE compiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with The CUTE compiler.  If not, see <https://www.gnu.org/licenses/>.
 */

use std::iter;

use llvm_ir::{instruction, terminator};
use llvm_ir::{Instruction, Operand, Terminator};

// TODO: upstream this if it makes sense
pub trait GetOperands {
    fn get_operands(&self) -> Vec<&Operand>;
}

impl GetOperands for Instruction {
    fn get_operands(&self) -> Vec<&Operand> {
        match self {
            Instruction::Add(instruction::Add {
                operand0, operand1, ..
            }) => vec![operand0, operand1],
            Instruction::Sub(instruction::Sub {
                operand0, operand1, ..
            }) => vec![operand0, operand1],
            Instruction::Mul(instruction::Mul {
                operand0, operand1, ..
            }) => vec![operand0, operand1],
            Instruction::UDiv(instruction::UDiv {
                operand0, operand1, ..
            }) => vec![operand0, operand1],
            Instruction::SDiv(instruction::SDiv {
                operand0, operand1, ..
            }) => vec![operand0, operand1],
            Instruction::URem(instruction::URem {
                operand0, operand1, ..
            }) => vec![operand0, operand1],
            Instruction::SRem(instruction::SRem {
                operand0, operand1, ..
            }) => vec![operand0, operand1],
            Instruction::And(instruction::And {
                operand0, operand1, ..
            }) => vec![operand0, operand1],
            Instruction::Or(instruction::Or {
                operand0, operand1, ..
            }) => vec![operand0, operand1],
            Instruction::Xor(instruction::Xor {
                operand0, operand1, ..
            }) => vec![operand0, operand1],
            Instruction::Shl(instruction::Shl {
                operand0, operand1, ..
            }) => vec![operand0, operand1],
            Instruction::LShr(instruction::LShr {
                operand0, operand1, ..
            }) => vec![operand0, operand1],
            Instruction::AShr(instruction::AShr {
                operand0, operand1, ..
            }) => vec![operand0, operand1],
            Instruction::FAdd(instruction::FAdd {
                operand0, operand1, ..
            }) => vec![operand0, operand1],
            Instruction::FSub(instruction::FSub {
                operand0, operand1, ..
            }) => vec![operand0, operand1],
            Instruction::FMul(instruction::FMul {
                operand0, operand1, ..
            }) => vec![operand0, operand1],
            Instruction::FDiv(instruction::FDiv {
                operand0, operand1, ..
            }) => vec![operand0, operand1],
            Instruction::FRem(instruction::FRem {
                operand0, operand1, ..
            }) => vec![operand0, operand1],
            Instruction::FNeg(instruction::FNeg { operand, .. }) => vec![operand],
            Instruction::ExtractElement(_) => unimplemented!(),
            Instruction::InsertElement(_) => unimplemented!(),
            Instruction::ShuffleVector(_) => unimplemented!(),
            Instruction::ExtractValue(_) => unimplemented!(),
            Instruction::InsertValue(_) => unimplemented!(),
            Instruction::Alloca(_) => vec![],
            Instruction::Load(instruction::Load { address, .. }) => vec![address],
            Instruction::Store(instruction::Store { address, value, .. }) => vec![address, value],
            Instruction::Fence(_) => unimplemented!(),
            Instruction::CmpXchg(_) => unimplemented!(),
            Instruction::AtomicRMW(_) => unimplemented!(),
            Instruction::GetElementPtr(instruction::GetElementPtr {
                address, indices, ..
            }) => iter::once(address)
                .chain(indices.iter())
                .collect::<Vec<_>>(), // TODO: clean this line
            Instruction::Trunc(instruction::Trunc { operand, .. }) => vec![operand],
            Instruction::ZExt(instruction::ZExt { operand, .. }) => vec![operand],
            Instruction::SExt(instruction::SExt { operand, .. }) => vec![operand],
            Instruction::FPTrunc(_) => unimplemented!(),
            Instruction::FPExt(_) => unimplemented!(),
            Instruction::FPToUI(_) => unimplemented!(),
            Instruction::FPToSI(_) => unimplemented!(),
            Instruction::UIToFP(_) => unimplemented!(),
            Instruction::SIToFP(_) => unimplemented!(),
            Instruction::PtrToInt(_) => unimplemented!(),
            Instruction::IntToPtr(_) => unimplemented!(),
            Instruction::BitCast(instruction::BitCast { operand, .. }) => vec![operand],
            Instruction::AddrSpaceCast(_) => unimplemented!(),
            Instruction::ICmp(instruction::ICmp {
                operand0, operand1, ..
            }) => vec![operand0, operand1],
            Instruction::FCmp(_) => unimplemented!(),
            Instruction::Phi(instruction::Phi {
                incoming_values, ..
            }) => incoming_values.iter().map(|(op, _)| op).collect(),
            Instruction::Select(instruction::Select {
                condition,
                true_value,
                false_value,
                ..
            }) => vec![condition, true_value, false_value],
            Instruction::Freeze(_) => unimplemented!(),
            Instruction::Call(instruction::Call { arguments, .. }) => {
                arguments.iter().map(|(op, _)| op).collect::<Vec<_>>()
            }
            Instruction::VAArg(_) => unimplemented!(),
            Instruction::LandingPad(_) => unimplemented!(),
            Instruction::CatchPad(_) => unimplemented!(),
            Instruction::CleanupPad(_) => unimplemented!(),
        }
    }
}

impl GetOperands for Terminator {
    fn get_operands(&self) -> Vec<&Operand> {
        match self {
            Terminator::Br(_) => vec![],
            Terminator::CondBr(terminator::CondBr { condition, .. }) => vec![condition],
            _ => unimplemented!(),
        }
    }
}

pub trait GetOperandsMut {
    fn get_operands_mut(&mut self) -> Vec<&mut Operand>;
}

impl GetOperandsMut for Instruction {
    fn get_operands_mut(&mut self) -> Vec<&mut Operand> {
        match self {
            Instruction::Add(instruction::Add {
                operand0, operand1, ..
            }) => vec![operand0, operand1],
            Instruction::Sub(instruction::Sub {
                operand0, operand1, ..
            }) => vec![operand0, operand1],
            Instruction::Mul(instruction::Mul {
                operand0, operand1, ..
            }) => vec![operand0, operand1],
            Instruction::UDiv(instruction::UDiv {
                operand0, operand1, ..
            }) => vec![operand0, operand1],
            Instruction::SDiv(instruction::SDiv {
                operand0, operand1, ..
            }) => vec![operand0, operand1],
            Instruction::URem(instruction::URem {
                operand0, operand1, ..
            }) => vec![operand0, operand1],
            Instruction::SRem(instruction::SRem {
                operand0, operand1, ..
            }) => vec![operand0, operand1],
            Instruction::And(instruction::And {
                operand0, operand1, ..
            }) => vec![operand0, operand1],
            Instruction::Or(instruction::Or {
                operand0, operand1, ..
            }) => vec![operand0, operand1],
            Instruction::Xor(instruction::Xor {
                operand0, operand1, ..
            }) => vec![operand0, operand1],
            Instruction::Shl(instruction::Shl {
                operand0, operand1, ..
            }) => vec![operand0, operand1],
            Instruction::LShr(instruction::LShr {
                operand0, operand1, ..
            }) => vec![operand0, operand1],
            Instruction::AShr(instruction::AShr {
                operand0, operand1, ..
            }) => vec![operand0, operand1],
            Instruction::FAdd(instruction::FAdd {
                operand0, operand1, ..
            }) => vec![operand0, operand1],
            Instruction::FSub(instruction::FSub {
                operand0, operand1, ..
            }) => vec![operand0, operand1],
            Instruction::FMul(instruction::FMul {
                operand0, operand1, ..
            }) => vec![operand0, operand1],
            Instruction::FDiv(instruction::FDiv {
                operand0, operand1, ..
            }) => vec![operand0, operand1],
            Instruction::FRem(instruction::FRem {
                operand0, operand1, ..
            }) => vec![operand0, operand1],
            Instruction::FNeg(instruction::FNeg { operand, .. }) => vec![operand],
            Instruction::ExtractElement(_) => unimplemented!(),
            Instruction::InsertElement(_) => unimplemented!(),
            Instruction::ShuffleVector(_) => unimplemented!(),
            Instruction::ExtractValue(_) => unimplemented!(),
            Instruction::InsertValue(_) => unimplemented!(),
            Instruction::Alloca(_) => vec![],
            Instruction::Load(instruction::Load { address, .. }) => vec![address],
            Instruction::Store(instruction::Store { address, value, .. }) => vec![address, value],
            Instruction::Fence(_) => unimplemented!(),
            Instruction::CmpXchg(_) => unimplemented!(),
            Instruction::AtomicRMW(_) => unimplemented!(),
            Instruction::GetElementPtr(instruction::GetElementPtr {
                address, indices, ..
            }) => iter::once(address)
                .chain(indices.iter_mut().map(|op| &mut *op))
                .collect::<Vec<_>>(), // TODO: clean this line
            Instruction::Trunc(instruction::Trunc { operand, .. }) => vec![operand],
            Instruction::ZExt(instruction::ZExt { operand, .. }) => vec![operand],
            Instruction::SExt(instruction::SExt { operand, .. }) => vec![operand],
            Instruction::FPTrunc(_) => unimplemented!(),
            Instruction::FPExt(_) => unimplemented!(),
            Instruction::FPToUI(_) => unimplemented!(),
            Instruction::FPToSI(_) => unimplemented!(),
            Instruction::UIToFP(_) => unimplemented!(),
            Instruction::SIToFP(_) => unimplemented!(),
            Instruction::PtrToInt(_) => unimplemented!(),
            Instruction::IntToPtr(_) => unimplemented!(),
            Instruction::BitCast(instruction::BitCast { operand, .. }) => vec![operand],
            Instruction::AddrSpaceCast(_) => unimplemented!(),
            Instruction::ICmp(instruction::ICmp {
                operand0, operand1, ..
            }) => vec![operand0, operand1],
            Instruction::FCmp(_) => unimplemented!(),
            Instruction::Phi(instruction::Phi {
                incoming_values, ..
            }) => incoming_values.iter_mut().map(|(op, _)| op).collect(),
            Instruction::Select(instruction::Select {
                condition,
                true_value,
                false_value,
                ..
            }) => vec![condition, true_value, false_value],
            Instruction::Freeze(_) => unimplemented!(),
            Instruction::Call(instruction::Call { arguments, .. }) => {
                arguments.iter_mut().map(|(op, _)| op).collect::<Vec<_>>()
            }
            Instruction::VAArg(_) => unimplemented!(),
            Instruction::LandingPad(_) => unimplemented!(),
            Instruction::CatchPad(_) => unimplemented!(),
            Instruction::CleanupPad(_) => unimplemented!(),
        }
    }
}

impl GetOperandsMut for Terminator {
    fn get_operands_mut(&mut self) -> Vec<&mut Operand> {
        match self {
            Terminator::Br(_) => vec![],
            Terminator::CondBr(terminator::CondBr { condition, .. }) => vec![condition],
            Terminator::Ret(terminator::Ret { return_operand, .. }) => match return_operand {
                Some(op) => vec![op],
                _ => vec![],
            },
            _ => unimplemented!(),
        }
    }
}
