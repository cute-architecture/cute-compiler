/*
 * Copyright © Romain Fouquet, 2020
 *
 * This file is part of the CUTE compiler.
 *
 * The CUTE compiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The CUTE compiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with The CUTE compiler.  If not, see <https://www.gnu.org/licenses/>.
 */

use llvm_ir::terminator;
use llvm_ir::{BasicBlock, Terminator};

pub trait CanBranchTo {
    fn can_branch_to(&self, block: &BasicBlock) -> bool;
}

impl CanBranchTo for BasicBlock {
    fn can_branch_to(&self, block: &BasicBlock) -> bool {
        match &self.term {
            Terminator::Br(terminator::Br { dest, .. }) => *dest == block.name,
            Terminator::CondBr(terminator::CondBr {
                true_dest,
                false_dest,
                ..
            }) => *true_dest == block.name || *false_dest == block.name,
            Terminator::Ret(_) => false,
            _ => unimplemented!(),
        }
    }
}
