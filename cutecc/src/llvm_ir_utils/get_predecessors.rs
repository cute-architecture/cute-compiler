/*
 * Copyright © Romain Fouquet, 2020
 *
 * This file is part of the CUTE compiler.
 *
 * The CUTE compiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The CUTE compiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with The CUTE compiler.  If not, see <https://www.gnu.org/licenses/>.
 */

use llvm_ir::terminator;
use llvm_ir::{BasicBlock, Function, Terminator};

pub trait GetPredecessors {
    fn get_predecessors<'a>(&self, function: &'a Function) -> Vec<&'a BasicBlock>;
}

impl GetPredecessors for BasicBlock {
    fn get_predecessors<'a>(&self, function: &'a Function) -> Vec<&'a BasicBlock> {
        function
            .basic_blocks
            .iter()
            .filter(|b| match &b.term {
                Terminator::Br(terminator::Br { dest, .. }) => *dest == self.name,
                Terminator::CondBr(terminator::CondBr {
                    true_dest,
                    false_dest,
                    ..
                }) => *true_dest == self.name || *false_dest == self.name,
                Terminator::Ret(_) => false,
                _ => unimplemented!(),
            })
            .collect::<Vec<_>>()
    }
}
