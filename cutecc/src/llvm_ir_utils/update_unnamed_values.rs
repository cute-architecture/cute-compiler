/*
 * Copyright © Romain Fouquet, 2020
 *
 * This file is part of the CUTE compiler.
 *
 * The CUTE compiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The CUTE compiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with The CUTE compiler.  If not, see <https://www.gnu.org/licenses/>.
 */

use llvm_ir::instruction;
use llvm_ir::{Instruction, Name, Operand, Terminator};

use super::get_operands::GetOperandsMut;
use super::get_result_mut::TryGetResultMut;

pub trait UpdateUnnamedValue {
    fn update_unnamed_value<F>(&mut self, update: F)
    where
        F: Fn(usize) -> usize;
}

impl UpdateUnnamedValue for Name {
    fn update_unnamed_value<F>(&mut self, update: F)
    where
        F: Fn(usize) -> usize,
    {
        if let Name::Number(number) = self {
            *number = update(*number);
        }
    }
}

impl UpdateUnnamedValue for Operand {
    fn update_unnamed_value<F>(&mut self, update: F)
    where
        F: Fn(usize) -> usize,
    {
        if let Self::LocalOperand { name, .. } = self {
            name.update_unnamed_value(&update);
        }
    }
}

pub trait UpdateUnnamedValues {
    fn update_unnamed_values<F>(&mut self, update: F)
    where
        F: Fn(usize) -> usize;
}

impl UpdateUnnamedValues for Instruction {
    fn update_unnamed_values<F>(&mut self, update: F)
    where
        F: Fn(usize) -> usize,
    {
        if let Some(name) = self.try_get_result_mut() {
            name.update_unnamed_value(&update);
        }

        for operand in self.get_operands_mut().into_iter() {
            operand.update_unnamed_value(&update);
        }

        if let Instruction::Phi(instruction::Phi {
            incoming_values, ..
        }) = self
        {
            for name in incoming_values.iter_mut().map(|(_, n)| n) {
                name.update_unnamed_value(&update);
            }
        }
    }
}

impl UpdateUnnamedValues for Terminator {
    fn update_unnamed_values<F>(&mut self, update: F)
    where
        F: Fn(usize) -> usize,
    {
        match self {
            Self::Br(br) => br.dest.update_unnamed_value(update),
            Self::CondBr(condbr) => {
                condbr.condition.update_unnamed_value(&update);
                condbr.true_dest.update_unnamed_value(&update);
                condbr.false_dest.update_unnamed_value(&update);
            }
            Self::Ret(ret) => {
                if let Some(operand) = ret.return_operand.as_mut() {
                    operand.update_unnamed_value(&update);
                }
            }
            _ => unimplemented!(),
        }
    }
}
