/*
 * Copyright © Romain Fouquet, 2020
 *
 * This file is part of the CUTE compiler.
 *
 * The CUTE compiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The CUTE compiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with The CUTE compiler.  If not, see <https://www.gnu.org/licenses/>.
 */

use llvm_ir::{BasicBlock, Function};

use super::get_predecessors::GetPredecessors;

trait GetPredecessorsChain {
    fn get_predecessors_chain<'a>(&self, function: &'a Function) -> Vec<&'a BasicBlock>;
}

impl GetPredecessorsChain for BasicBlock {
    fn get_predecessors_chain<'a>(&self, function: &'a Function) -> Vec<&'a BasicBlock> {
        let mut predecessors = vec![];
        let mut current_predecessors = self.get_predecessors(function);
        let mut current_depth = 0;

        loop {
            current_depth += 1;
            predecessors.extend(current_predecessors.clone());
            current_predecessors = current_predecessors
                .iter()
                .flat_map(|b| b.get_predecessors(function))
                .collect::<Vec<_>>();
            current_predecessors.sort_by_key(|b| b.name.clone());
            current_predecessors.dedup();

            // If this self is in the current_predecessors
            if let Some(index) = current_predecessors.iter().position(|&b| b == self) {
                current_predecessors.remove(index);
            }

            // If the first self in the function is in the current_predecessors
            if let Some(index) = current_predecessors
                .iter()
                .position(|&b| function.basic_blocks.iter().position(|bb| bb == b) == Some(0))
            {
                current_predecessors.remove(index);
            }

            current_predecessors = current_predecessors
                .iter()
                .filter(|b| !predecessors.iter().any(|bb| bb == *b))
                .copied()
                .collect();

            if current_predecessors.is_empty() {
                break;
            }
        }

        predecessors
    }
}
