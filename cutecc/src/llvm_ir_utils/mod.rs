/*
 * Copyright © Romain Fouquet, 2020
 *
 * This file is part of the CUTE compiler.
 *
 * The CUTE compiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The CUTE compiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with The CUTE compiler.  If not, see <https://www.gnu.org/licenses/>.
 */

use std::iter;

use llvm_ir::{BasicBlock, Function, Instruction, Name, Operand, Terminator};

pub mod get_operands;
pub mod print_ir;
use get_operands::{GetOperands, GetOperandsMut};
pub mod get_result_mut;
use get_result_mut::TryGetResultMut;
mod update_unnamed_values;
use update_unnamed_values::{UpdateUnnamedValue, UpdateUnnamedValues};
pub mod can_branch_to;
pub mod get_name_str;
pub mod get_predecessors;
pub mod get_predecessors_chain;
pub mod uses;

trait GetUnnamedValueCounterAtPos {
    fn get_unnamed_value_counter_at_pos(&self, basic_block_index: usize, pos: usize) -> usize;
}

impl GetUnnamedValueCounterAtPos for Function {
    /// Search the unnamed value just before pos among instruction results, block names and
    /// function parameters
    fn get_unnamed_value_counter_at_pos(&self, basic_block_index: usize, pos: usize) -> usize {
        match self
            .parameters
            .iter()
            .map(|p| Some(&p.name))
            .chain(
                self.basic_blocks
                    .iter()
                    .take(basic_block_index)
                    .flat_map(|b| {
                        iter::once(Some(&b.name)).chain(b.instrs.iter().map(|i| {
                            match i.try_get_result() {
                                // TODO: do not call the function again
                                Some(Name::Number(_)) => i.try_get_result(),
                                _ => None,
                            }
                        }))
                    })
                    .chain(
                        iter::once(Some(&self.basic_blocks[basic_block_index].name)).chain(
                            self.basic_blocks[basic_block_index]
                                .instrs
                                .iter()
                                .take(pos)
                                .map(|i| match i.try_get_result() {
                                    // TODO: do not call the function again
                                    Some(Name::Number(_)) => i.try_get_result(),
                                    _ => None,
                                }),
                        ),
                    ),
            )
            .rfind(|r| matches!(r, Some(Name::Number(_))))
        {
            Some(Some(Name::Number(number))) => *number,
            _ => {
                dbg!(&basic_block_index, pos);
                // FIXME: can this actually happen?
                // Can happen if first block name is a named value
                0
            }
        }
    }
}

pub trait InsertInstruction {
    fn insert_instruction(
        &mut self,
        basic_block_index: usize,
        instruction_index: usize,
        instruction: &Instruction,
    ) -> Instruction;
}

impl InsertInstruction for Function {
    // TODO: take a slice of Instruction instead and change update increment
    fn insert_instruction(
        &mut self,
        basic_block_index: usize,
        instruction_index: usize,
        instruction: &Instruction,
    ) -> Instruction {
        let mut instruction = instruction.clone();

        if let Some(Name::Number(number)) = instruction.try_get_result_mut() {
            *number =
                self.get_unnamed_value_counter_at_pos(basic_block_index, instruction_index) + 1
        }

        // If the instruction introduced a new unnamed value
        if let Some(Name::Number(inserted_result)) = instruction.try_get_result() {
            let increment_if_new = |n| if n >= *inserted_result { n + 1 } else { n };

            {
                let mut next_basic_blocks = self.basic_blocks.iter_mut();
                next_basic_blocks.nth(basic_block_index);

                // Update basic block unnamed values
                for basic_block in &mut next_basic_blocks {
                    basic_block.name.update_unnamed_value(increment_if_new);
                }
            }

            // Update instruction unnamed values
            for instr in &mut self.basic_blocks.iter_mut().flat_map(|b| &mut b.instrs) {
                instr.update_unnamed_values(increment_if_new);
            }

            // Update terminators unnamed values
            for term in self.basic_blocks.iter_mut().map(|b| &mut b.term) {
                term.update_unnamed_values(increment_if_new);
            }
        }

        // Insert the instruction
        self.basic_blocks
            .get_mut(basic_block_index)
            .unwrap()
            .instrs
            .insert(instruction_index, instruction.clone());

        instruction
    }
}

pub trait RemoveInstruction {
    fn remove_instruction(
        &mut self,
        basic_block_index: usize,
        instruction_index: usize,
        new_operand: Option<&Operand>,
    );
}

// TODO: factor this out with InsertInstruction
impl RemoveInstruction for Function {
    // TODO: take a slice of Instruction instead and change update increment
    fn remove_instruction(
        &mut self,
        basic_block_index: usize,
        instruction_index: usize,
        new_operand: Option<&Operand>,
    ) {
        // Remove the instruction
        let removed_instruction = self
            .basic_blocks
            .get_mut(basic_block_index)
            .unwrap()
            .instrs
            .remove(instruction_index);

        // If the removed instruction was introducing a new value
        if let Some(removed_result) = removed_instruction.try_get_result() {
            if let Some(new_operand) = new_operand {
                // Update operands
                for instr in &mut self.basic_blocks.iter_mut().flat_map(|b| &mut b.instrs) {
                    for operand in instr.get_operands_mut() {
                        if let Operand::LocalOperand { name, .. } = operand {
                            if name == removed_result {
                                *operand = new_operand.clone();
                            }
                        }
                    }
                }

                // Update terminator operands
                for term in self.basic_blocks.iter_mut().map(|b| &mut b.term) {
                    for operand in term.get_operands_mut() {
                        if let Operand::LocalOperand { name, .. } = operand {
                            if name == removed_result {
                                *operand = operand.clone();
                            }
                        }
                    }
                }
            } else {
                // If the removed instruction was introducing a new unnamed value
                if let Name::Number(removed_result) = removed_result {
                    let decrement_if_removed = |n| if n >= *removed_result { n - 1 } else { n };

                    {
                        let mut next_basic_blocks = self.basic_blocks.iter_mut();
                        next_basic_blocks.nth(basic_block_index);

                        // Update basic block unnamed values
                        for basic_block in &mut next_basic_blocks {
                            basic_block.name.update_unnamed_value(decrement_if_removed);
                        }
                    }

                    // Update instruction unnamed values
                    for instr in &mut self.basic_blocks.iter_mut().flat_map(|b| &mut b.instrs) {
                        instr.update_unnamed_values(decrement_if_removed);
                    }

                    // Update terminators unnamed values
                    for term in self.basic_blocks.iter_mut().map(|b| &mut b.term) {
                        term.update_unnamed_values(decrement_if_removed);
                    }
                }
            }
        }
    }
}

pub trait IsDefinedUsedIn {
    fn is_defined_in(&self, basic_block: &BasicBlock) -> bool;
    fn is_used_in(&self, basic_block: &BasicBlock) -> bool;
}

impl IsDefinedUsedIn for Name {
    fn is_defined_in(&self, basic_block: &BasicBlock) -> bool {
        basic_block
            .instrs
            .iter()
            .any(|i| i.try_get_result() == Some(self))
    }

    fn is_used_in(&self, basic_block: &BasicBlock) -> bool {
        basic_block.instrs.iter().any(|i| {
            i.get_operands().iter().any(|op| match op {
                Operand::LocalOperand { name, .. } => name == self,
                _ => false,
            })
        }) || match &basic_block.term {
            Terminator::Br(br) => br.dest == *self,
            Terminator::CondBr(condbr) => match &condbr.condition {
                Operand::LocalOperand { name, .. } => name == self,
                _ => false,
            },
            Terminator::Ret(ret) => match &ret.return_operand {
                Some(Operand::LocalOperand { name, .. }) => name == self,
                _ => false,
            },
            _ => unimplemented!(),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use pretty_assertions::assert_eq;

    use llvm_ir::instruction;
    use llvm_ir::{Constant, ConstantRef, Module};

    #[test]
    fn test_insert_remove_instruction() {
        let module =
            Module::from_bc_path("tests/fixtures/uart_led_blink.bc").expect("cannot parse module");

        let mut function_main = module.get_func_by_name("main").unwrap().clone();

        for insert_in_basic_block in 0..function_main.basic_blocks.len() {
            for insert_at_pos in 0..function_main.basic_blocks[insert_in_basic_block]
                .instrs
                .len()
            {
                let add_instruction = Instruction::Add(instruction::Add {
                    operand0: Operand::ConstantOperand(ConstantRef::new(Constant::Int {
                        bits: 32,
                        value: 2000,
                    })),
                    operand1: Operand::ConstantOperand(ConstantRef::new(Constant::Int {
                        bits: 32,
                        value: 2001,
                    })),
                    dest: Name::Number(0),
                    debugloc: None,
                });

                let old_function_main = function_main.clone();
                function_main.insert_instruction(
                    insert_in_basic_block,
                    insert_at_pos,
                    &add_instruction,
                );
                let function_main_insert = function_main.clone();
                function_main.remove_instruction(insert_in_basic_block, insert_at_pos, None);

                assert_ne!(old_function_main, function_main_insert);
                assert_eq!(old_function_main, function_main);
            }
        }
    }
}
