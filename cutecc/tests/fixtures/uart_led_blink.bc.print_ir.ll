source_filename = "cutecc/tests/fixtures/uart_led_blink.c"
target datalayout = "e-m:e-p:32:32-Fi8-i64:64-v128:64:128-a:0:32-n32-S64"
target triple = "armv4t-unknown-unknown"

@.str = private unnamed_addr constant [8 x i8] [ i8 99, i8 117, i8 116, i8 101, i8 0, i8 0, i8 0, i8 0 ], align 1

; Function Attrs: noinline nounwind "frame-pointer"="all" "min-legal-vector-width"="0" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="arm7tdmi" "target-features"="+armv4t,+soft-float,+strict-align,-aes,-bf16,-d32,-dotprod,-fp-armv8,-fp-armv8d16,-fp-armv8d16sp,-fp-armv8sp,-fp16,-fp16fml,-fp64,-fpregs,-fullfp16,-mve,-mve.fp,-neon,-sha2,-thumb-mode,-vfp2,-vfp2sp,-vfp3,-vfp3d16,-vfp3d16sp,-vfp3sp,-vfp4,-vfp4d16,-vfp4d16sp,-vfp4sp" "use-soft-float"="true"
define arm_aapcscc void @main() {
  store i32 305419896, inttoptr (i32 66 to i32*), align 4
  br label %1

1:                                                ; preds = %0, %20
  br label %NodeBlock3

NodeBlock3:                                       ; preds = %1, %20
  %2 = phi i8 [ i8 0, %1 ], [ i8 %21, %20 ]
  %Pivot4 = icmp ult i8 %2, i8 2
  br i1 %Pivot4, label %NodeBlock, label %NodeBlock1

NodeBlock1:                                       ; preds = %NodeBlock3
  %Pivot2 = icmp eq i8 %2, i8 2
  br i1 %Pivot2, label %8, label %LeafBlock

LeafBlock:                                        ; preds = %NodeBlock1
  %SwitchLeaf = icmp eq i8 %2, i8 3
  br i1 %SwitchLeaf, label %12, label %NewDefault

NodeBlock:                                        ; preds = %NodeBlock3
  %Pivot = icmp eq i8 %2, i8 0
  br i1 %Pivot, label %3, label %7

3:                                                ; preds = %NodeBlock
  %4 = zext i8 %2 to i32
  %5 = getelementptr inbounds [8 x i8]* @.str, i32 0, i32 %4
  %6 = load i8* %5, align 1
  store i8 %6, inttoptr (i32 2147532800 to i8*), align 16384
  store i8 102, inttoptr (i32 2147516416 to i8*), align 32768
  br label %NewDefault

7:                                                ; preds = %NodeBlock
  store i8 117, inttoptr (i32 2147532800 to i8*), align 16384
  store i8 103, inttoptr (i32 2147516416 to i8*), align 32768
  br label %NewDefault

8:                                                ; preds = %NodeBlock1
  %9 = zext i8 %2 to i32
  %10 = getelementptr inbounds [8 x i8]* @.str, i32 0, i32 %9
  %11 = load i8* %10, align 1
  store i8 %11, inttoptr (i32 2147532800 to i8*), align 16384
  store i8 102, inttoptr (i32 2147516416 to i8*), align 32768
  br label %NewDefault

12:                                               ; preds = %LeafBlock
  %13 = zext i8 %2 to i32
  %14 = getelementptr inbounds [8 x i8]* @.str, i32 0, i32 %13
  %15 = load i8* %14, align 1
  store i8 %15, inttoptr (i32 2147532800 to i8*), align 16384
  store i8 103, inttoptr (i32 2147516416 to i8*), align 32768
  br label %NewDefault

NewDefault:                                       ; preds = %LeafBlock, %3, %7, %8, %12
  br label %16

16:                                               ; preds = %NewDefault, %16
  %17 = phi i32 [ i32 0, %NewDefault ], [ i32 %18, %16 ]
  %18 = add i32 %17, i32 1
  %19 = icmp ult i32 %18, i32 2
  br i1 %19, label %16, label %20

20:                                               ; preds = %16
  %21 = add i8 %2, i8 1
  %22 = icmp ult i8 %21, i8 4
  br i1 %22, label %NodeBlock3, label %1
}
