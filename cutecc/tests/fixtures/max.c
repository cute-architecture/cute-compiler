void main() {
  unsigned int* gpio = (unsigned int*) 0x80008000;
  *gpio = 0x66;

  unsigned int list[] = {2, 5, 1, 7, 9,
                         0x67, 4, 4, 3, 5};
  const unsigned int LIST_LENGTH = 10;

  unsigned int max = list[0];
  for (unsigned int i = 0; i < LIST_LENGTH; i++) {
    if (list[i] > max)
      max = list[i];
  }

  *gpio = max;

  while (1);
}
