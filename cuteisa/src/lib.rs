/*
 * Copyright © Romain Fouquet, 2020
 *
 * This file is part of the CUTE compiler.
 *
 * The CUTE compiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The CUTE compiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with The CUTE compiler.  If not, see <https://www.gnu.org/licenses/>.
 */

#[macro_use]
extern crate lazy_static;

#[macro_use]
extern crate maplit;

use std::collections::HashMap;

/// Number of bits of a block header
pub const BLOCK_HEADER_SIZE: u8 = 32;

/// Number of bits of an instruction
pub const INSTRUCTION_SIZE: u8 = 32;

/// Number of bits of a subbclock descriptor
pub const SUBBLOCK_DESCRIPTOR_SIZE: u8 = 32;

/// Number of local registers in each execution node.
pub const LOCAL_REG_NB: u16 = 16;

/// Number of global registers in the processor core.
pub const GLOBAL_REG_NB: u16 = 256;

/// Maximum number of sub-blocks (blocks that a block can branch to).
pub const MAX_SUBBLOCKS_NB: u8 = 16;

/// Maximum Maximum Execution Depth
pub const MAX_MAX_EXEC_DEPTH: u8 = 255;

/// Maximum number of input instructions (`READ` and `LDR` instructions) of a block.
pub const MAX_BLOCK_INPUTS: u16 = LOCAL_REG_NB;

/// Maximum number of body instructions in a block.
pub const MAX_BODY_LENGTH: usize = 64; // TODO: maybe rename this

/// Maximum length of a forward jump of a `JMP` instruction.
pub const MAX_JUMP_LENGTH: u8 = 7; // TODO: maybe rename this

// TODO: replace WORD with INSTRUCTION?
/// Number of 8-bit bytes in a word.
pub const WORD_SIZE_BYTES: usize = 4;

/// Number of 8-bit bytes in an immediate value.
pub const IMMEDIATE_SIZE_BYTES: usize = WORD_SIZE_BYTES / 2;

/// Number of bits in byte.
pub const BYTE_SIZE: usize = 8;

/// Number of bits in an immediate value.
pub const IMMEDIATE_SIZE: usize = IMMEDIATE_SIZE_BYTES * BYTE_SIZE;

// TODO: merge this value with the previous one?
/// Upper immediate value shift
pub const CUTE_UPPER_IMM_SHIFT: usize = 16;

pub const MEM_FLASH_OFFSET: u32 = 0x4000_0000;
pub const MEM_FLASH_OFFSET_START_CODE: u32 = 0x20fbc; // FIXME: this should not be necessary

pub fn can_be_lower_immediate_value(value: u64) -> bool {
    // TODO: use the same comparison as in can_be_short_immediate_value
    value == (value as u16) as u64
}

pub fn can_be_upper_immediate_value(value: u64) -> bool {
    value == (value >> 16) << 16
}

pub fn can_be_short_immediate_value(value: u64) -> bool {
    value == (value << 8) >> 8
}

lazy_static! {
    /// Number of bits needed to store the local register number.
    pub static ref LOCAL_REG_WIDTH: u8 = (LOCAL_REG_NB as f32).log2().ceil() as u8;
}

lazy_static! {
    /// Number of bits needed to store the global register number.
    pub static ref GLOBAL_REG_WIDTH: u8 = (GLOBAL_REG_NB as f32).log2().ceil() as u8;
}

const BLOCK_EXEC_MODE_SHIFT: u8 = 6;

lazy_static! {
    pub static ref BLOCK_EXEC_MODE: HashMap<&'static str, u8> = hashmap! {
        "NOEXEC" => 0x00 << BLOCK_EXEC_MODE_SHIFT,
        "NOSPEC" => 0x01 << BLOCK_EXEC_MODE_SHIFT,
        "LOOP"   => 0x02 << BLOCK_EXEC_MODE_SHIFT,
        "SPEC"   => 0x03 << BLOCK_EXEC_MODE_SHIFT,
    };
}

pub const IS_IMMEDIATE_CODE: u8 = 0x01 << 2;
pub const IS_UPPER_CODE: u8 = 0x01;

lazy_static! {
    pub static ref CONDITION_MNEMONICS: HashMap<&'static str, u8> = hashmap! {
        "EQ" => 0x00,
        "NE" => 0x01,
        "ULT" => 0x02,
        "ULE" => 0x03,
        "UGT" => 0x04,
        "UGE" => 0x05,
        "SLT" => 0x06,
        "SLE" => 0x07,
        "SGT" => 0x08,
        "SGE" => 0x09,
    };
}
