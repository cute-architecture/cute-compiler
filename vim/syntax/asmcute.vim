" Maintainer: Romain Fouquet
" License: This file is distributed under the GPLv3 license.

if exists("b:current_syntax")
  finish
endif

setlocal iskeyword=48-57,a-z,A-Z,#,$,-

function! s:addwordmatches(synGroup, patterns) abort
  for l:pat in a:patterns
    execute 'syn match ' . a:synGroup . " '\\<" . l:pat . "\\>'"
  endfor
endfunction

let asmcuteImmediateValues = [
      \'#\d\+',
      \'\$[0-9a-fA-F]\+',
      \'b[01]\+',
      \]

call s:addwordmatches('Constant', asmcuteImmediateValues)

syn match asmcuteLocalRegister 'L\d\+'

syn match asmcuteGlobalRegister 'R\d\+'

let asmcuteBodyMnemonics = [
      \'ADD\(UI\|I\)\?',
      \'SUB\(UI\|I\)\?',
      \'XOR\(UI\|I\)\?',
      \'AND\(UI\|I\)\?',
      \'OR\(UI\|I\)\?',
      \'CMPI\?',
      \'JMPI\?',
      \'STR\(UI\|I\)\?',
      \'WRITE\(UI\|I\)\?',
      \'BRI\?',
      \'NOP',
      \]

call s:addwordmatches('asmcuteBodyMnemonic', asmcuteBodyMnemonics)

let asmcuteInputMnemonics = [
      \'READ',
      \'LDRB\?',
      \]

call s:addwordmatches('asmcuteInputMnemonic', asmcuteInputMnemonics)

let asmcuteComparisonTypes = [
      \'EQ',
      \'NE',
      \'[US][LG]T',
      \]

call s:addwordmatches('asmcuteComparisonType', asmcuteComparisonTypes)

let asmcuteExecModes = [
      \'NOEXEC',
      \'NOSPEC',
      \'LOOP',
      \'SPEC',
      \]

call s:addwordmatches('asmcuteExecModes', asmcuteExecModes)

syn match asmcuteLabel '^[a-zA-Z0-9$_.]\+:'

" FIXME: do not match labels that start with a dot
syn match asmcuteDirective '^\s*\.[a-zA-Z]\+'

syn match asmcuteOperator '[,\[\]()]'

syn match Comment ';.*' contains=@Spell

hi! link asmcuteLabel           Define

hi! link asmcuteDirective       Type

hi! link asmcuteOperator        Special

hi! link asmcuteLocalRegister   Identifier
hi! link asmcuteGlobalRegister  Define

hi! link asmcuteInputMnemonic   Type
hi! link asmcuteComparisonType  Type
hi! link asmcuteExecModes       Type

hi! link asmcuteBodyMnemonic    Type
