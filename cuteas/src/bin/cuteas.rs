/*
 * Copyright © Romain Fouquet, 2020
 *
 * This file is part of the CUTE compiler.
 *
 * The CUTE compiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The CUTE compiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with The CUTE compiler.  If not, see <https://www.gnu.org/licenses/>.
 */

use std::fs;
use std::io::Write;

use clap::Parser;
use std::path::PathBuf;

#[derive(Parser)]
#[clap(about = "Assembler for the CUTE ISA assembly language.")]
struct Args {
    /// Input file
    #[clap(short = 'f', parse(from_os_str))]
    input_file: PathBuf,

    /// Output file
    #[clap(short = 'o', parse(from_os_str))]
    output_file: PathBuf,

    /// Hex file
    #[clap(long = "hex", parse(from_os_str))]
    hex_file: Option<PathBuf>,
    // TODO: maybe add an option to disable warnings
}

fn main() {
    let args = Args::parse();

    let assembly = fs::read_to_string(args.input_file).expect("cannot read file");

    let program = cuteas::parse_assembly(&assembly).unwrap_or_else(|e| panic!("{}", e));

    let program_code = cuteas::generate_program_code(&program).unwrap();

    let mut output_file = fs::File::create(args.output_file).expect("cannot open file to write");
    output_file
        .write_all(&program_code.0)
        .expect("cannot write to file");

    if let Some(hex_file) = args.hex_file {
        fs::write(hex_file, &program_code.1).expect("cannot open file to write");
    }

    println!("{}", program_code.1);
}
