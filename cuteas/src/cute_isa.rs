/*
 * Copyright © Romain Fouquet, 2020
 *
 * This file is part of the CUTE compiler.
 *
 * The CUTE compiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The CUTE compiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with The CUTE compiler.  If not, see <https://www.gnu.org/licenses/>.
 */

use std::collections::HashMap;

// TODO: move this in cuteisa

use super::Rule;

const INPUT_OPCODE_SHIFT: u8 = 3;
const BODY_OPCODE_SHIFT: u8 = 3;

lazy_static! {
    pub static ref OPCODE: HashMap<super::Rule, u8> = hashmap!{
        // Input instructions
        Rule::read_instruction            => 0x01 << INPUT_OPCODE_SHIFT,
        Rule::ldr_instruction             => 0x02 << INPUT_OPCODE_SHIFT,
        Rule::ldrb_instruction             => 0x03 << INPUT_OPCODE_SHIFT,
        // Body instructions
        Rule::operation_mnemonic_add      => 0x11 << BODY_OPCODE_SHIFT,
        Rule::operation_mnemonic_sub      => 0x12 << BODY_OPCODE_SHIFT,
        Rule::operation_mnemonic_xor      => 0x14 << BODY_OPCODE_SHIFT,
        Rule::operation_mnemonic_and      => 0x18 << BODY_OPCODE_SHIFT,
        Rule::operation_mnemonic_or       => 0x13 << BODY_OPCODE_SHIFT,
        Rule::cmp_instruction             => 0x01 << BODY_OPCODE_SHIFT,
        Rule::cmp_instruction_immediate   => 0x01 << BODY_OPCODE_SHIFT,
        Rule::br_instruction              => 0x02 << BODY_OPCODE_SHIFT,
        Rule::br_instruction_immediate    => 0x02 << BODY_OPCODE_SHIFT,
        Rule::write_instruction           => 0x04 << BODY_OPCODE_SHIFT,
        Rule::write_instruction_immediate => 0x04 << BODY_OPCODE_SHIFT,
        Rule::str_instruction             => 0x03 << BODY_OPCODE_SHIFT,
        Rule::str_instruction_immediate   => 0x03 << BODY_OPCODE_SHIFT,
    };
}
