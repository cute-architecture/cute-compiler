/*
 * Copyright © Romain Fouquet, 2020
 *
 * This file is part of the CUTE compiler.
 *
 * The CUTE compiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The CUTE compiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with The CUTE compiler.  If not, see <https://www.gnu.org/licenses/>.
 */

#[macro_use]
extern crate pest_derive;

#[macro_use]
extern crate lazy_static;

#[macro_use]
extern crate maplit;

mod cute_isa;

use std::collections::HashMap;
use std::convert::TryInto;
use std::num::ParseIntError;
use std::str::FromStr;

use pest::Parser;

use cute_isa::*;
use cuteisa::*;

#[derive(Debug)]
pub enum ErrorVariant {
    InconsistentBlockID,
    RegisterNumberTooLarge,
    JumpTooLong,
    BIndexTooLarge,
    SubblockNotFound,
    // TODO: rename this to 'Illegal'
    // Bugs
    UnknownExecMode,
    UnknownOpcode,
    UnknownConditionType,
}

#[derive(Debug)]
pub struct GenerateError {
    pub variant: ErrorVariant,
    pub element: u32,
    // TODO: add location and better messages
}

#[derive(Debug)]
pub enum WarningVariant {
    TooManySubblocks,
    TooManyBlockInputs,
    TooManyBodyInstructions,
}

#[derive(Debug)]
pub struct GenerateWarning {
    pub variant: WarningVariant,
    pub block_label: String,
}

struct Word {
    src: String,
    code: Box<[u8; WORD_SIZE_BYTES]>,
}

type ImmediateValue = Box<[u8; IMMEDIATE_SIZE_BYTES]>;

type BlockOffset = u32;
type SubblockIndex = u8;

#[derive(Debug)]
struct BlockText {
    label: String,
    src: String,
    offset: BlockOffset,
    block_code: Vec<u8>,
    subblock_descriptor_list: Vec<SubblockDescriptor>,
    warnings: Vec<GenerateWarning>,
}

#[derive(Debug)]
struct SubblockDescriptor {
    label: String,
    offset: BlockOffset,
    execution_mode: String,
    subblock_index: SubblockIndex,
    subblock_descriptor_code: Vec<u8>,
}

#[derive(Debug)]
struct BlockObject {
    label: String,
    src: String,
    offset: BlockOffset,
    block_code: Vec<u8>,
    warnings: Vec<GenerateWarning>,
}

fn generate_subblock_descriptor(
    subblock_descriptor: &SubblockDescriptor,
) -> Result<SubblockDescriptor, GenerateError> {
    let subblock_execution_mode = subblock_descriptor.execution_mode.clone();

    let exec_mode_code = BLOCK_EXEC_MODE
        .get::<str>(&subblock_execution_mode.to_uppercase())
        .ok_or(GenerateError {
            variant: ErrorVariant::UnknownExecMode,
            element: 0,
        })?;

    let offset = subblock_descriptor.offset;

    let subblock_descriptor_code = vec![
        *exec_mode_code,
        (offset >> (2 * 8)) as u8,
        (offset >> 8) as u8,
        offset as u8,
    ];

    Ok(SubblockDescriptor {
        label: subblock_descriptor.label.clone(),
        offset,
        execution_mode: subblock_descriptor.execution_mode.clone(),
        subblock_index: subblock_descriptor.subblock_index,
        subblock_descriptor_code,
    })
}

// TODO: take inner_rules as param instead and return an Option<u8> (can be zero)
fn get_register_number(register: pest::iterators::Pair<Rule>) -> u8 {
    u8::from_str(register.into_inner().next().unwrap().as_str()).unwrap()
}

fn check_register_size(register_number: u8, max_register_number: u16) -> Result<(), GenerateError> {
    if register_number as u16 > max_register_number - 1 {
        return Err(GenerateError {
            variant: ErrorVariant::RegisterNumberTooLarge,
            element: register_number as u32,
        });
    }

    Ok(())
}

fn generate_input_instruction(
    instruction: pest::iterators::Pair<Rule>,
) -> Result<Word, GenerateError> {
    let mut inner_rules = instruction.clone().into_inner();

    let input_instruction = inner_rules.next().unwrap();

    let opcode = *OPCODE
        .get(&input_instruction.as_rule())
        .ok_or(GenerateError {
            variant: ErrorVariant::UnknownOpcode,
            element: 0,
        })?;

    let mut input_instruction_inner_rules = input_instruction.into_inner();

    let local_register_number = get_register_number(input_instruction_inner_rules.next().unwrap());
    check_register_size(local_register_number, LOCAL_REG_NB)?;

    let global_register_number = get_register_number(input_instruction_inner_rules.next().unwrap());
    check_register_size(global_register_number, GLOBAL_REG_NB)?;

    Ok(Word {
        src: instruction.as_str().to_string(),
        code: Box::new([
            opcode,
            0x00,
            global_register_number >> *LOCAL_REG_WIDTH,
            (global_register_number << *LOCAL_REG_WIDTH) | local_register_number,
        ]),
    })
}

fn generate_operation_instruction(
    instruction: pest::iterators::Pair<Rule>,
) -> Result<Word, GenerateError> {
    let mut inner_rules = instruction.clone().into_inner();

    let opcode = *OPCODE
        .get(
            &inner_rules
                .next()
                .unwrap()
                .into_inner()
                .next()
                .unwrap()
                .as_rule(),
        )
        .ok_or(GenerateError {
            variant: ErrorVariant::UnknownOpcode,
            element: 0,
        })?;

    let destination_local_register = get_register_number(inner_rules.next().unwrap());
    check_register_size(destination_local_register, LOCAL_REG_NB)?;

    let source_local_register_a = get_register_number(inner_rules.next().unwrap());
    check_register_size(source_local_register_a, LOCAL_REG_NB)?;

    let source_local_register_b = get_register_number(inner_rules.next().unwrap());
    check_register_size(source_local_register_b, LOCAL_REG_NB)?;

    Ok(Word {
        src: instruction.as_str().to_string(),
        code: Box::new([
            opcode,
            0x00,
            source_local_register_a,
            (destination_local_register << *LOCAL_REG_WIDTH) | source_local_register_b,
        ]),
    })
}

fn get_immediate_value(
    value: pest::iterators::Pair<Rule>,
) -> Result<ImmediateValue, ParseIntError> {
    let mut inner_rules = value.into_inner();
    let integer = inner_rules.next().unwrap();

    let integer_rule = integer.as_rule();
    let integer_value = integer.into_inner().next().unwrap().as_str();

    let integer_number = match integer_rule {
        Rule::decimal_value => integer_value.parse::<u16>().unwrap(),
        Rule::hex_value => u16::from_str_radix(integer_value, 16).unwrap(),
        Rule::bin_value => u16::from_str_radix(integer_value, 2).unwrap(),
        _ => unreachable!(),
    };

    let immediate_value = Box::new([integer_number as u8, (integer_number >> 8) as u8]);
    Ok(immediate_value)
}

fn generate_operation_instruction_immediate(
    instruction: pest::iterators::Pair<Rule>,
) -> Result<Word, GenerateError> {
    let mut inner_rules = instruction.clone().into_inner();

    let operation_mnemonic = inner_rules.next().unwrap();

    let is_upper_code = match inner_rules.next().unwrap().as_rule() {
        Rule::immediate_suffix => 0x00,
        Rule::upper_immediate_suffix => IS_UPPER_CODE,
        _ => unreachable!(),
    };

    let opcode = *OPCODE
        .get(&operation_mnemonic.into_inner().next().unwrap().as_rule())
        .ok_or(GenerateError {
            variant: ErrorVariant::UnknownOpcode,
            element: 0,
        })?;

    let is_immediate_code = IS_IMMEDIATE_CODE;

    let destination_local_register = get_register_number(inner_rules.next().unwrap());
    check_register_size(destination_local_register, LOCAL_REG_NB)?;

    let source_local_register = get_register_number(inner_rules.next().unwrap());
    check_register_size(source_local_register, LOCAL_REG_NB)?;

    let immediate_value = get_immediate_value(inner_rules.next().unwrap()).unwrap();

    Ok(Word {
        src: instruction.as_str().to_string(),
        code: Box::new([
            opcode | is_immediate_code | is_upper_code,
            immediate_value[1],
            immediate_value[0],
            (destination_local_register << *LOCAL_REG_WIDTH) | source_local_register,
        ]),
    })
}

fn generate_cmp_instruction(
    instruction: pest::iterators::Pair<Rule>,
) -> Result<Word, GenerateError> {
    let opcode = *OPCODE.get(&instruction.as_rule()).ok_or(GenerateError {
        variant: ErrorVariant::UnknownOpcode,
        element: 0,
    })?;

    let mut inner_rules = instruction.clone().into_inner();

    let destination_local_register = get_register_number(inner_rules.next().unwrap());
    check_register_size(destination_local_register, LOCAL_REG_NB)?;

    let source_local_register_a = get_register_number(inner_rules.next().unwrap());
    check_register_size(source_local_register_a, LOCAL_REG_NB)?;

    let condition_mnemonic = CONDITION_MNEMONICS
        .get::<str>(&inner_rules.next().unwrap().as_str().to_uppercase())
        .ok_or(GenerateError {
            variant: ErrorVariant::UnknownConditionType,
            element: 0,
        })?;

    let source_local_register_b = get_register_number(inner_rules.next().unwrap());
    check_register_size(source_local_register_b, LOCAL_REG_NB)?;

    // FIXME: check condition_mnemonic
    Ok(Word {
        src: instruction.as_str().to_string(),
        code: Box::new([
            opcode | ((*condition_mnemonic & 0xc) >> 2),
            *condition_mnemonic << 6,
            source_local_register_a,
            (destination_local_register << *LOCAL_REG_WIDTH) | source_local_register_b,
        ]),
    })
}

fn generate_cmp_instruction_immediate(
    instruction: pest::iterators::Pair<Rule>,
) -> Result<Word, GenerateError> {
    let opcode = *OPCODE.get(&instruction.as_rule()).ok_or(GenerateError {
        variant: ErrorVariant::UnknownOpcode,
        element: 0,
    })?;

    let mut inner_rules = instruction.clone().into_inner();
    inner_rules.next(); // Skip the suffix

    let destination_local_register = get_register_number(inner_rules.next().unwrap());
    check_register_size(destination_local_register, LOCAL_REG_NB)?;

    let source_local_register = get_register_number(inner_rules.next().unwrap());
    check_register_size(source_local_register, LOCAL_REG_NB)?;

    let condition_mnemonic = CONDITION_MNEMONICS
        .get::<str>(&inner_rules.next().unwrap().as_str().to_uppercase())
        .ok_or(GenerateError {
            variant: ErrorVariant::UnknownConditionType,
            element: 0,
        })?;

    let is_immediate_code = IS_IMMEDIATE_CODE;

    let immediate_value = get_immediate_value(inner_rules.next().unwrap()).unwrap();

    Ok(Word {
        src: instruction.as_str().to_string(),
        code: Box::new([
            opcode | is_immediate_code | ((condition_mnemonic & 0xc) >> 2),
            condition_mnemonic << 6,
            // immediate_value[1],
            immediate_value[0],
            (destination_local_register << *LOCAL_REG_WIDTH) | source_local_register,
        ]),
    })
}

fn generate_br_instruction(
    instruction: pest::iterators::Pair<Rule>,
) -> Result<Word, GenerateError> {
    let opcode = *OPCODE.get(&instruction.as_rule()).ok_or(GenerateError {
        variant: ErrorVariant::UnknownOpcode,
        element: 0,
    })?;

    let is_immediate_instr = matches!(instruction.as_rule(), Rule::br_instruction_immediate);

    let mut inner_rules = instruction.clone().into_inner();

    inner_rules.next(); // Skip the block label
    inner_rules.next(); // Skip the block index
    let subblock_index_f = u8::from_str(inner_rules.next().unwrap().as_str()).unwrap() << 4;

    match is_immediate_instr {
        true => Ok(Word {
            src: instruction.as_str().to_string(),
            code: Box::new([opcode, 0x00, 0x00, subblock_index_f]),
        }),
        false => {
            let source_local_register = get_register_number(inner_rules.next().unwrap());
            check_register_size(source_local_register, LOCAL_REG_NB)?;

            inner_rules.next(); // Skip the block label
            inner_rules.next(); // Skip the execution mode
            let subblock_index_t = u8::from_str(inner_rules.next().unwrap().as_str()).unwrap();

            Ok(Word {
                src: instruction.as_str().to_string(),
                code: Box::new([
                    opcode,
                    0x00,
                    subblock_index_t,
                    subblock_index_f | source_local_register,
                ]),
            })
        }
    }
}

fn generate_write_instruction(
    instruction: pest::iterators::Pair<Rule>,
) -> Result<Word, GenerateError> {
    let opcode = *OPCODE.get(&instruction.as_rule()).ok_or(GenerateError {
        variant: ErrorVariant::UnknownOpcode,
        element: 0,
    })?;

    let mut inner_rules = instruction.clone().into_inner();

    let destination_global_register = get_register_number(inner_rules.next().unwrap());
    check_register_size(destination_global_register, GLOBAL_REG_NB)?;

    let source_local_register = get_register_number(inner_rules.next().unwrap());
    check_register_size(source_local_register, LOCAL_REG_NB)?;

    Ok(Word {
        src: instruction.as_str().to_string(),
        code: Box::new([
            opcode,
            0x00,
            source_local_register,
            destination_global_register,
        ]),
    })
}

fn generate_write_instruction_immediate(
    instruction: pest::iterators::Pair<Rule>,
) -> Result<Word, GenerateError> {
    let opcode = *OPCODE.get(&instruction.as_rule()).ok_or(GenerateError {
        variant: ErrorVariant::UnknownOpcode,
        element: 0,
    })?;

    let is_immediate_code = IS_IMMEDIATE_CODE;

    let mut inner_rules = instruction.clone().into_inner();
    inner_rules.next(); // Skip the suffix

    let destination_global_register = get_register_number(inner_rules.next().unwrap());
    check_register_size(destination_global_register, GLOBAL_REG_NB)?;

    let immediate_value = get_immediate_value(inner_rules.next().unwrap()).unwrap();

    Ok(Word {
        src: instruction.as_str().to_string(),
        code: Box::new([
            opcode | is_immediate_code,
            immediate_value[1],
            immediate_value[0],
            destination_global_register,
        ]),
    })
}

fn generate_str_instruction(
    instruction: pest::iterators::Pair<Rule>,
) -> Result<Word, GenerateError> {
    let opcode = *OPCODE.get(&instruction.as_rule()).ok_or(GenerateError {
        variant: ErrorVariant::UnknownOpcode,
        element: 0,
    })?;

    let mut inner_rules = instruction.clone().into_inner();

    let destination_address_local_register = get_register_number(inner_rules.next().unwrap());
    check_register_size(destination_address_local_register, LOCAL_REG_NB)?;

    let source_local_register = get_register_number(inner_rules.next().unwrap());
    check_register_size(source_local_register, LOCAL_REG_NB)?;

    Ok(Word {
        src: instruction.as_str().to_string(),
        code: Box::new([
            opcode,
            0x00,
            0x00,
            source_local_register << *LOCAL_REG_WIDTH | destination_address_local_register,
        ]),
    })
}

fn generate_str_instruction_immediate(
    instruction: pest::iterators::Pair<Rule>,
) -> Result<Word, GenerateError> {
    let opcode = *OPCODE.get(&instruction.as_rule()).ok_or(GenerateError {
        variant: ErrorVariant::UnknownOpcode,
        element: 0,
    })?;

    let is_immediate_code = IS_IMMEDIATE_CODE;

    let mut inner_rules = instruction.clone().into_inner();
    inner_rules.next(); // Skip the suffix

    let destination_address_local_register = get_register_number(inner_rules.next().unwrap());
    check_register_size(destination_address_local_register, LOCAL_REG_NB)?;

    let immediate_value = get_immediate_value(inner_rules.next().unwrap()).unwrap();

    Ok(Word {
        src: instruction.as_str().to_string(),
        code: Box::new([
            opcode | is_immediate_code,
            immediate_value[1],
            immediate_value[0],
            destination_address_local_register,
        ]),
    })
}

fn generate_nop_instruction() -> Result<Word, GenerateError> {
    // TODO: leverage generate_operation_instruction
    // ADDI L0, L0, L0
    Ok(Word {
        src: "NOP".to_string(),
        code: Box::new([
            *OPCODE.get(&Rule::operation_mnemonic_add).unwrap(),
            0x00,
            0x00,
            0x00,
        ]),
    })
}

fn generate_body_instruction(
    instruction: pest::iterators::Pair<Rule>,
) -> Result<Word, GenerateError> {
    match instruction.as_rule() {
        Rule::operation_instruction => generate_operation_instruction(instruction),
        Rule::operation_instruction_immediate => {
            generate_operation_instruction_immediate(instruction)
        }
        Rule::cmp_instruction => generate_cmp_instruction(instruction),
        Rule::cmp_instruction_immediate => generate_cmp_instruction_immediate(instruction),
        Rule::br_instruction | Rule::br_instruction_immediate => {
            generate_br_instruction(instruction)
        }
        Rule::write_instruction => generate_write_instruction(instruction),
        Rule::write_instruction_immediate => generate_write_instruction_immediate(instruction),
        Rule::str_instruction => generate_str_instruction(instruction),
        Rule::str_instruction_immediate => generate_str_instruction_immediate(instruction),
        Rule::nop_instruction => generate_nop_instruction(),
        _ => unreachable!(),
    }
}

fn parse_br_instruction(
    instruction: pest::iterators::Pair<Rule>,
) -> Result<Vec<SubblockDescriptor>, ()> {
    // FIXME: add an error type
    let mut inner_rules = instruction.clone().into_inner();

    let label_f = inner_rules.next().unwrap().as_str().to_string();
    let execution_mode_f = inner_rules.next().unwrap().as_str().to_string();
    let subblock_index_f = u8::from_str(inner_rules.next().unwrap().as_str()).unwrap();

    match instruction.as_rule() {
        Rule::br_instruction_immediate => Ok(vec![SubblockDescriptor {
            label: label_f,
            offset: 0,
            execution_mode: execution_mode_f,
            subblock_index: subblock_index_f,
            subblock_descriptor_code: vec![],
        }]),
        Rule::br_instruction => {
            inner_rules.next(); // Slip local register
            let label_t = inner_rules.next().unwrap().as_str().to_string();
            let execution_mode_t = inner_rules.next().unwrap().as_str().to_string();
            let subblock_index_t = u8::from_str(inner_rules.next().unwrap().as_str()).unwrap();

            Ok(vec![
                SubblockDescriptor {
                    label: label_f,
                    offset: 0,
                    execution_mode: execution_mode_f,
                    subblock_index: subblock_index_f,
                    subblock_descriptor_code: vec![],
                },
                SubblockDescriptor {
                    label: label_t,
                    offset: 0,
                    execution_mode: execution_mode_t,
                    subblock_index: subblock_index_t,
                    subblock_descriptor_code: vec![],
                },
            ])
        }
        _ => unreachable!(),
    }
}

fn generate_block_header(
    max_exec_depth: u8,
    input_count: u8,
    body_length: u8,
    footer_length: u8,
) -> Vec<u8> {
    vec![
        max_exec_depth,
        input_count << 4 | (body_length >> 3),
        body_length << 5 | footer_length << 1,
        0x00,
    ]
}

fn print_word_hex(src: &str, code: &[u8]) -> String {
    format!(
        "{:0pad$x}{:0pad$x}{:0pad$x}{:0pad$x} // {}\n",
        code[0],
        code[1],
        code[2],
        code[3],
        src,
        pad = 2
    )
}

fn generate_block_text(
    block: pest::iterators::Pair<Rule>,
) -> Result<BlockText, Vec<GenerateError>> {
    let mut inner_rules = block.into_inner();

    let block_label_line = inner_rules.next().unwrap();
    let block_label = block_label_line.into_inner().next().unwrap().as_str();

    let next_rule = inner_rules.next().unwrap();
    let (block_max_exec_depth, mut block_input_list) = match next_rule.as_rule() {
        Rule::block_max_exec_depth => (
            u8::from_str(next_rule.as_str()).unwrap(),
            inner_rules.next().unwrap().into_inner(),
        ),
        _ => (0, next_rule.into_inner()),
    };

    let mut generate_errors: Vec<GenerateError> = vec![];
    let mut warnings: Vec<GenerateWarning> = vec![];

    let mut block_input_list_code: Vec<u8> = vec![];
    let mut block_hex = String::new();
    let mut input_count = 0;

    for _ in 0..MAX_BLOCK_INPUTS {
        let input_instruction = block_input_list.next();
        match input_instruction {
            Some(instruction) => match generate_input_instruction(instruction) {
                Ok(Word { src, code }) => {
                    block_input_list_code.extend(code.iter());
                    block_hex += &print_word_hex(&src, &*code);
                    input_count += 1;
                }
                Err(generate_error) => generate_errors.push(generate_error),
            },
            _ => break,
        }
    }

    // TODO: add a warning if the same register is written to twice

    if block_input_list.next().is_some() {
        warnings.push(GenerateWarning {
            variant: WarningVariant::TooManyBlockInputs,
            block_label: block_label.to_string(),
        });
    }

    let mut block_body = inner_rules.next().unwrap().into_inner();
    let mut block_subblock_list: Vec<SubblockDescriptor> = vec![];

    let mut body_instruction_list_code: Vec<u8> = vec![];
    let mut body_length = 0;

    for _ in 0..MAX_BODY_LENGTH {
        let body_instruction = block_body.next();
        match body_instruction {
            Some(body_instruction) => {
                let instruction = body_instruction.into_inner().next().unwrap();

                if let Rule::br_instruction | Rule::br_instruction_immediate = instruction.as_rule()
                {
                    let subblock_descriptors = parse_br_instruction(instruction.clone()).unwrap();
                    block_subblock_list.extend(subblock_descriptors);
                }

                match generate_body_instruction(instruction) {
                    Ok(Word { src, code }) => {
                        body_instruction_list_code.extend(code.iter());
                        block_hex += &print_word_hex(&src, &*code);
                        body_length += 1;
                    }
                    Err(generate_error) => generate_errors.push(generate_error),
                }
            }
            _ => break,
        }
    }

    // FIXME: this should be an error
    if block_body.next().is_some() {
        warnings.push(GenerateWarning {
            variant: WarningVariant::TooManyBodyInstructions,
            block_label: block_label.to_string(),
        });
    }

    let footer_length = block_subblock_list.len();

    // FIXME: this should be an error
    if footer_length > MAX_SUBBLOCKS_NB as usize {
        warnings.push(GenerateWarning {
            variant: WarningVariant::TooManySubblocks,
            block_label: block_label.to_string(),
        });
    }

    let block_header = generate_block_header(
        block_max_exec_depth,
        input_count,
        body_length,
        footer_length as u8,
    );
    block_hex = print_word_hex(
        &format!(
            "{}; Maximum Execution Depth: {}, Input count: {}, Body length: {}, Footer length: {}",
            block_label, block_max_exec_depth, input_count, body_length, footer_length,
        ),
        &block_header,
    ) + &block_hex;

    let block_code = block_header
        .into_iter()
        .chain(
            block_input_list_code
                .into_iter()
                .chain(body_instruction_list_code.into_iter()),
        )
        .collect();

    // TODO: deduplicate subblocks if possible

    block_subblock_list.sort_by_key(|s| s.subblock_index);

    match generate_errors.is_empty() {
        true => Ok(BlockText {
            label: block_label.to_string(),
            src: block_hex,
            offset: 0,
            subblock_descriptor_list: block_subblock_list,
            block_code,
            warnings,
        }),
        false => Err(generate_errors),
    }
}

pub fn parse_assembly(
    assembly: &str,
) -> Result<pest::iterators::Pair<Rule>, pest::error::Error<Rule>> {
    Ok(CuteParser::parse(Rule::program, assembly)?.next().unwrap())
}

fn generate_text_code(
    program: pest::iterators::Pair<Rule>,
) -> Result<(Vec<u8>, String), Vec<GenerateError>> {
    let blocks = program
        .into_inner()
        .filter(|block| block.as_rule() == Rule::block_text)
        .map(generate_block_text)
        .collect::<Vec<Result<_, Vec<GenerateError>>>>();

    let has_error = blocks.iter().any(|res| res.is_err());

    if has_error {
        return Err(blocks
            .into_iter()
            .filter_map(|res| res.err())
            .flatten()
            .collect::<Vec<GenerateError>>());
    }

    let mut blocks = blocks
        .into_iter()
        .map(|res| res.unwrap())
        .collect::<Vec<_>>();

    let mut offset = 0;

    for mut block in &mut blocks {
        block.offset = offset;

        // offset += (block.block_code.len()/WORD_SIZE_BYTES) as BlockOffset;
        offset += block.block_code.len() as BlockOffset;
        // offset += block.subblock_descriptor_list.len() as BlockOffset;
        offset += (block.subblock_descriptor_list.len() * WORD_SIZE_BYTES) as BlockOffset;
    }

    // TODO: check no two blocks have the same labels

    let label_offset_hashmap = blocks
        .iter()
        .map(|b| (b.label.to_string(), b.offset))
        .collect::<HashMap<_, _>>();

    // Resolve subblock offsets based on labels
    for block in &mut blocks {
        for subblock in &mut block.subblock_descriptor_list {
            match label_offset_hashmap.get(&subblock.label) {
                Some(target_offset) => subblock.offset = *target_offset,
                None => {
                    return Err(vec![GenerateError {
                        variant: ErrorVariant::SubblockNotFound,
                        element: 0,
                    }])
                }
            }
        }
    }

    for block in &mut blocks {
        for subblock in &block.subblock_descriptor_list {
            // TODO: handle UnknownExecMode
            let subblock_descriptor = generate_subblock_descriptor(subblock).unwrap();
            block
                .block_code
                .extend(&subblock_descriptor.subblock_descriptor_code);
            block.src += &print_word_hex(
                &format!(
                    "({} @{:#04x}, {}, {})",
                    subblock_descriptor.label,
                    subblock_descriptor.offset, // FIXME
                    subblock_descriptor.execution_mode,
                    subblock_descriptor.subblock_index,
                ),
                &subblock_descriptor.subblock_descriptor_code,
            );
        }
    }

    let block_list = &mut blocks
        .iter()
        .flat_map(|b| {
            b.subblock_descriptor_list
                .iter()
                .map(|sd| (sd.label.clone(), sd.offset))
        })
        .collect::<Vec<_>>();
    block_list.sort_by_key(|(_, offset)| *offset);
    block_list.dedup();
    let block_list = block_list
        .iter()
        .map(|(label, offset)| format!("{:#06x}: {}", offset, label))
        .collect::<Vec<_>>()
        .join("\n");

    println!("{}", block_list);

    let text_hex = blocks
        .iter()
        .map(|res| res.src.to_string())
        .collect::<Vec<_>>()
        .join("\n");

    Ok((
        blocks
            .iter()
            .flat_map(|block| {
                vec![
                    block.block_code.clone(),
                    block
                        .subblock_descriptor_list
                        .iter()
                        .flat_map(|s| s.subblock_descriptor_code.clone())
                        .collect(),
                ]
                .into_iter()
                .flatten()
            })
            .collect(),
        text_hex,
    ))
}

fn generate_block_rodata(
    block: pest::iterators::Pair<Rule>,
) -> Result<BlockObject, Vec<GenerateError>> {
    let block_rule = block.as_rule();
    let mut inner_rules = block.into_inner();

    let block_label_line = inner_rules.next().unwrap();
    let block_label = block_label_line.into_inner().next().unwrap().as_str();

    let warnings: Vec<GenerateWarning> = vec![];

    let block_code_hex = match block_rule {
        Rule::block_object_word => inner_rules
            .enumerate()
            .map(|(i, r)| {
                let directive = r.clone();
                let code = match directive.as_rule() {
                    Rule::directive_word => {
                        u32::from_str(directive.into_inner().next().unwrap().as_str())
                            .unwrap()
                            .to_be_bytes()
                    }
                    _ => unimplemented!(),
                };
                (
                    code,
                    print_word_hex(&format!("{}[{}]: {}", block_label, i, r.as_str(),), &code),
                )
            })
            .collect::<Vec<_>>(),
        Rule::block_object_byte => inner_rules
            .enumerate()
            .collect::<Vec<_>>()
            .chunks(INSTRUCTION_SIZE as usize / BYTE_SIZE)
            .map(|chunk| {
                let code: [u8; 4] = chunk
                    .iter()
                    .map(|r| {
                        let directive = r.1.clone();
                        u8::from_str(directive.clone().into_inner().next().unwrap().as_str())
                            .unwrap()
                    })
                    .collect::<Vec<_>>()[..]
                    .try_into()
                    .expect("cannot produce aligned word");
                (
                    code,
                    print_word_hex(
                        &format!(
                            "{}[{}..{}]: {{{}, {}, {}, {}}}",
                            block_label,
                            chunk[0].0 + INSTRUCTION_SIZE as usize / BYTE_SIZE - 1,
                            chunk[0].0,
                            code[3],
                            code[2],
                            code[1],
                            code[0],
                        ),
                        &code,
                    ),
                )
            })
            .collect::<Vec<_>>(),
        _ => unimplemented!(),
    };

    Ok(BlockObject {
        label: block_label.to_string(),
        src: block_code_hex
            .iter()
            .map(|(_, s)| s.clone())
            .collect::<Vec<_>>()
            .join(""),
        offset: 0, // FIXME
        block_code: block_code_hex
            .iter()
            .flat_map(|(c, _)| c.iter())
            .copied()
            .collect::<Vec<_>>(),
        warnings,
    })
}

fn generate_rodata_code(
    program: pest::iterators::Pair<Rule>,
) -> Result<(Vec<u8>, String), Vec<GenerateError>> {
    let blocks = program
        .into_inner()
        .filter(|block| {
            block.as_rule() == Rule::block_object_word || block.as_rule() == Rule::block_object_byte
        })
        .map(generate_block_rodata)
        .collect::<Vec<Result<_, Vec<GenerateError>>>>();

    let has_error = blocks.iter().any(|res| res.is_err());

    if has_error {
        return Err(blocks
            .into_iter()
            .filter_map(|res| res.err())
            .flatten()
            .collect::<Vec<GenerateError>>());
    }

    let blocks = blocks
        .into_iter()
        .map(|res| res.unwrap())
        .collect::<Vec<_>>();

    let rodata_hex = blocks
        .iter()
        .map(|res| res.src.to_string())
        .collect::<Vec<_>>()
        .join("\n");

    Ok((
        blocks
            .iter()
            .flat_map(|block| block.block_code.clone())
            .collect(),
        rodata_hex,
    ))
}

pub fn generate_program_code(
    program: &pest::iterators::Pair<Rule>,
) -> Result<(Vec<u8>, String), Vec<GenerateError>> {
    let text_code = generate_text_code(program.clone())?;
    let rodata_code = generate_rodata_code(program.clone())?;

    Ok((
        text_code
            .0
            .iter()
            .chain(rodata_code.0.iter())
            .copied()
            .collect::<Vec<_>>(),
        format!(
            "{}{}",
            text_code.1,
            (if rodata_code.1.is_empty() { "" } else { "\n" }).to_string() + &rodata_code.1,
        ),
    ))
}

// TODO: add a function that takes assembly and returns binary code directly

#[derive(Parser)]
#[grammar = "cute_grammar.pest"]
struct CuteParser;

#[cfg(test)]
mod tests {
    use super::*;

    use std::fs;

    use insta::assert_snapshot;
    use pretty_assertions::assert_eq;

    #[test]
    fn program_code_generation() {
        let assembly =
            fs::read_to_string("tests/fixtures/uart_led_blink.cute").expect("cannot read file");

        let program = parse_assembly(&assembly).unwrap();

        let program_code = generate_program_code(&program).unwrap();

        assert_snapshot!(program_code.1);
    }

    // TODO: write more tests, especially to check max allowed values

    #[test]
    fn test_generate_subblock_descriptor() {
        let subblock_descriptor = SubblockDescriptor {
            label: "test".to_string(),
            offset: 0x123456,
            execution_mode: "NOSPEC".to_string(),
            subblock_index: 0,
            subblock_descriptor_code: vec![],
        };

        let generated_subblock_descriptor =
            generate_subblock_descriptor(&subblock_descriptor).unwrap();

        assert_eq!(
            generated_subblock_descriptor.subblock_descriptor_code,
            vec![0x40, 0x12, 0x34, 0x56]
        );
    }

    #[test]
    fn test_generate_block_header() {
        assert_eq!(
            generate_block_header(6, 2, 42, 3),
            vec![0x06, 0x25, 0x46, 0x00]
        );
    }
}
