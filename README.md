# The CUTE Compiler

> The CUTE (CUTting Edge) architecture is intended to be a modern, innovative
ISA originally inspired by the EDGE architecture class.

## Running

### Requirements

LLVM is required to compile the CUTE Compiler:

```sh
apt install llvm-14-dev
```

Read the CUTE compiler help with:
```sh
cargo run --bin cutecc -- --help
```

Usage example:
```sh
cargo run --bin cutecc -- cutecc/tests/fixtures/uart_led_blink.c -o cutecc/tests/fixtures/uart_led_blink.bin --hex cutecc/tests/fixtures/uart_led_blink.cute.hex -S cutecc/tests/fixtures/uart_led_blink.cute
```

## License
Source code is licensed under the GPL 3.0 license or later, see `COPYING`.
Documentation, logos and diagrams are licensed under
[CC BY-SA 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/).
